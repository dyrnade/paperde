/*
  *
  * This file is a part of PaperSessionManager.
  * PaperSessionManager is the Session Manager for PaperDesktop
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of PaperDesktop.
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#pragma once

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtDBus/QtDBus>
#include <QtCore/qcontainerfwd.h>

class Power;

class SessionAdaptor : public QDBusAbstractAdaptor {
	Q_OBJECT;
	Q_CLASSINFO("D-Bus Interface", "org.CuboCore.Paper.Session");
	Q_CLASSINFO("D-Bus Introspection", ""
									   "<interface name='org.CuboCore.Paper.Session'>\n"
									   "   <method name='Logout'/>\n"
									   "   <method name='Suspend'/>\n"
									   "   <method name='Hibernate'/>\n"
									   "   <method name='Shutdown'/>\n"
									   "   <method name='Reboot'/>\n"
									   "   <method name='CanSuspend'>\n"
									   "       <arg type=\"b\" direction=\"out\"/>\n"
									   "   </method>\n"
									   "   <method name='CanHibernate'>\n"
									   "       <arg type=\"b\" direction=\"out\"/>\n"
									   "   </method>\n"
									   "   <method name='CanShutdown'>\n"
									   "       <arg type=\"b\" direction=\"out\"/>\n"
									   "   </method>\n"
									   "   <method name='CanReboot'>\n"
									   "       <arg type=\"b\" direction=\"out\"/>\n"
									   "   </method>\n"
									   "</interface>"
				);

public:
	SessionAdaptor(QObject *parent);
	virtual ~SessionAdaptor();

public Q_SLOTS:
	void Logout();
	void Suspend();
	void Hibernate();
	void Shutdown();
	void Reboot();

	bool CanSuspend();
	bool CanHibernate();
	bool CanShutdown();
	bool CanReboot();

private:
	Power *pwr;
};

class PaperAdaptor : public QDBusAbstractAdaptor {
	Q_OBJECT;
	Q_CLASSINFO("D-Bus Interface", "org.CuboCore.Paper");
	Q_CLASSINFO("D-Bus Introspection", ""
									   "<interface name='org.CuboCore.Paper'>\n"
									   "   <method name='RegisterService'>\n"
									   "       <arg name=\"service\" type=\"s\" direction=\"in\"/>\n"
									   "   </method>\n"
									   "   <method name='RegisteredServices'>\n"
									   "       <arg type=\"as\" direction=\"out\"/>\n"
									   "   </method>\n"
									   "</interface>"
				);

public:
	PaperAdaptor(QObject *parent);
	virtual ~PaperAdaptor();

public Q_SLOTS:
	void RegisterService(QString);
	QStringList RegisteredServices();

private:
	QStringList services;
};
