/*
  *
  * This file is a part of PaperSessionManager.
  * PaperSessionManager is the Session Manager for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of Paper Desktop.
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#include <QDir>
#include <QProcess>
#include <QSettings>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>

#include <cprime/filefunc.h>
#include <cprime/systemxdg.h>

#include "sessionmgr.h"
#include "sessionadaptor.h"

#include <paperde/paperlog.h>
#include <paper-config.h>

#include <DFL/DF6/Login1.hpp>
#include <DFL/DF6/CoreApplication.hpp>

inline static QString xdgRuntimeDir()
{
	/* Get it from the env variable */
	QString dir(qgetenv("XDG_RUNTIME_DIR"));

	/* Variable not set */
	if (not dir.count())
	{
		dir = QString("/run/user/%1").arg(getuid());

		/* We will try to create /run/$UID */
		if (CPrime::FileUtils::mkpath(dir, 0700))
		{
			return dir + (dir.endsWith("/") ? "" : "/");
		}
	}

	/* So the variable is set, but does not exist */
	else if (CPrime::FileUtils::exists(dir))
	{
		/* Created successfully */
		if (CPrime::FileUtils::mkpath(dir, 0700))
		{
			return dir + (dir.endsWith("/") ? "" : "/");
		}
	}

	/** We failed with /run/user/$USER. Time for /tmp/$USER */
	dir = QString("/tmp/%1/").arg(getuid());
	if (CPrime::FileUtils::mkpath(dir, 0700))
	{
		return dir;
	}

	return QString();
}


sessionmanager::sessionmanager()
	: QObject()
	, wfPID(-1)
{
	QDBusConnection::sessionBus().registerService("org.CuboCore.Paper");
	QDBusConnection::sessionBus().registerObject("/org/CuboCore/Paper", this);

	paper = new PaperAdaptor(this);

	QDBusConnection::sessionBus().registerService("org.CuboCore.Paper.Session");
	QDBusConnection::sessionBus().registerObject("/org/CuboCore/Paper/Session", this);

	pwr = new SessionAdaptor(this);

	paper->RegisterService("org.CuboCore.Paper.Session");

	session = new QSettings("paperde", "papersession");

	login1 = new DFL::Login1(this);

	/** Acquire only those locks which the user specifies. 127 is the default -> Acquire all locks. */
	login1->acquireInhibitLocks(DFL::Login1::InhibitorLocks(session->value("AcquireLocks", 127).toInt()));
}


void sessionmanager::startSession()
{
	/* Setup the environmental variables */
	setupEnvironmentVars();

	/* Start the window manager and hence the shell */
	startWayfire();
}


void sessionmanager::stop()
{
	qDebug() << "Stoping Paper Session Manager";

	/** Release the inhibitor locks */
	login1->releaseInhibitLocks();

	/** Kill wayfire */
	kill(wfPID, SIGKILL);

	/* Kill this process. */
	qApp->quit();
}


void sessionmanager::handleMessages(const QString& msg, int fd)
{
	if (msg.startsWith("Can"))
	{
		int ret = login1->request(msg);

		if ((ret == -2) or (ret == 0))
		{
			qApp->messageClient("false", fd);
		}

		else if ((ret == -1) or (ret == 1))
		{
			qApp->messageClient("true", fd);
		}

		else
		{
			qWarning() << "Invalid query";
			qApp->messageClient("false", fd);
		}

		return;
	}

	if (msg == "logout")
	{
		stop();
	}

	else if (msg == "shutdown")
	{
		login1->request("PowerOff");
		stop();
	}

	else if (msg == "reboot")
	{
		login1->request("Reboot");
		stop();
	}

	else if (msg == "suspend")
	{
		login1->request("Suspend");
	}

	else if (msg == "hibernate")
	{
		login1->request("Hibernate");
	}

	// else if (msg.startsWith("session"))
	// {
	// 	QStringList args = msg.split(" ", Qt::SkipEmptyParts);
	// 	if (args.count() == 2)
	// 	{
	// 		startSession(args[1]);
	// 	}
	//
	// 	else if (args.count() == 1)
	// 	{
	// 		startSession();
	// 	}
	// }
}


void sessionmanager::setupEnvironmentVars()
{
	/**
	  *  Check if we have a writable '/tmp'.
	  *  Otherwise, we should gracefull exit.
	  */
	if (not CPrime::FileUtils::isWritable("/tmp"))
	{
		qCritical() << "Unable to write in /tmp. Exiting.";
		qApp->quit();
	}

	qDebug() << "Setting environment variables...";

	/* Let wayland perform the decoration? */
	qunsetenv("QT_WAYLAND_DECORATION");

	/* Wayland or X11 */
	qputenv("XDG_SESSION_TYPE", QByteArrayLiteral("wayland"));

	/** XDG_RUNTIME_DIR is not set.
	  *  We simply call xdgRuntimeDir().
	  *  It's guaranteed to give us an existing location if we have a sane system.
	  *  If it returns an empty dir, it means /tmp/ is not writable and we should
	  *  quit.
	  */
	if (not qgetenv("XDG_RUNTIME_DIR").count())
	{
		QString X_R_D = xdgRuntimeDir();
		if (not X_R_D.isEmpty())
		{
			qputenv("XDG_RUNTIME_DIR", X_R_D.toUtf8());
		}

		else
		{
			qApp->quit();
		}
	}

	/* Platform for Qt5 */
	qputenv("QT_QPA_PLATFORM", QByteArrayLiteral("wayland"));

	/* Qt5 theme to be used */
	qputenv("QT_QPA_PLATFORMTHEME", QByteArrayLiteral("qt5ct"));

	/* Perform Client side decoration: Eventually, we should provide server-side decoration */
	qputenv("QT_WAYLAND_DISABLE_DECORATION", QByteArrayLiteral("1"));

	/* Run firefox in wayland */
	qputenv("MOZ_ENABLE_WAYLAND", QByteArrayLiteral("1"));

	/* Desktop session name */
	qputenv("XDG_SESSION_DESKTOP", QByteArrayLiteral("PaperDE"));
	qputenv("XDG_CURRENT_DESKTOP", QByteArrayLiteral("PaperDE"));

	/* Desktop session */
	qputenv("DESKTOP_SESSION", QByteArrayLiteral("/usr/share/wayland-sessions/paperdesktop"));

	/* $XDG_RUNTIME_DIR/PaperSession-$XDG_SESSION_ID/ */
	/* This ensures that all the sockets will be in our */
	/* Session folder. So multiple logins of the same user */
	/* will be isolated. */

	/* Get the $XDG_WORK_DIR. By now it should be available. */
	QString workDir = qgetenv("XDG_RUNTIME_DIR");

	/* Now create the folder: Session$XDG_SESSION_ID inside /tmp/$USER/ */
	workDir += "/PaperSession-" + qgetenv("XDG_SESSION_ID") + "/";
	CPrime::FileUtils::mkpath(workDir, 0700);
	qputenv("__PAPER_WORK_DIR", workDir.toUtf8());

	/* Temp workspace for current Paper Session */
	qDebug() << "Paper Session work directory" << workDir;

	/** User defined environment variables */
	session->beginGroup("Environment");
	for ( QString var: session->allKeys())
	{
		qputenv(var.toUtf8(), session->value(var).toString().toUtf8());
	}

	session->endGroup();
}


void sessionmanager::startWayfire()
{
	qDebug() << "Starting window manager: wayfire";

	wfConfig = QDir::home().filePath(".config/paperde/wayfire.ini");
	if (not QFile::exists(wfConfig))
	{
		bool ok = true;
		ok &= QDir::home().mkpath(".config/paperde/");
		ok &= QFile::copy("/usr/share/paperde/wayfire.ini", wfConfig);

		/* If we're unable to copy the config file to user's config dir, use the default */
		if (not ok)
		{
			qDebug() << "Error copying wayfire.ini to ~/.config/paperde/";
			wfConfig = "/usr/share/paperde/wayfire.ini";
		}
	}

	qputenv("WAYFIRE_CONFIG_FILE", wfConfig.toUtf8().constData());

	/**Create a tracked process for wayfire */
	wfProc = new QProcess(this);
	wfProc->setWorkingDirectory(QDir::homePath());
	wfRestartCount = 0;

	/**
	  * Since paper-session is responsible for starting/stopping wayfire,
	  * any termination of wayfire will be treated as a crash. We will
	  * attempt to restart it upto 4 times: in all wayfire is started
	  * five times. After 5 attempts at starting wayfire, we will give up
	  * and end the session.
	  * A corresponding error message will show up in Paper Shell. This
	  * is based on __WAYFIRE_RESTART_COUNT env variable.
	  */
	connect(
		wfProc, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), [ = ](int, QProcess::ExitStatus) mutable {
		if (wfRestartCount < 4)
		{
			wfRestartCount++;
			qputenv("__WAYFIRE_RESTART_COUNT", QByteArray::number(wfRestartCount));
			wfProc->start("wayfire", { "--config", wfConfig });

			/** Wait for the process to start and retrieve it's PID */
			qDebug() << "Restarting wayfire" << wfProc->waitForStarted();
			wfPID = wfProc->processId();
			qputenv("__WF_PID", QByteArray::number(wfPID));
		}

		else
		{
			qDebug() << "Wayfire has crashed 5 times. Maximum retries exceeded.";
			qDebug() << "Closing session.";
			stop();
		}
	}
		);

	qputenv("__WAYFIRE_RESTART_COUNT", QByteArray::number(wfRestartCount));
	wfProc->start("wayfire", { "--config", wfConfig });

	/** Wait for the process to start and retrieve it's PID */
	qDebug() << "Starting wayfire" << wfProc->waitForStarted();
	wfPID = wfProc->processId();

	/**If wayfire isn't running, no point in continuing */
	if (wfPID == 0)
	{
		qDebug() << "Wayfire is not running... Quitting the session";
		qApp->quit();
		return;
	}

	qputenv("__WF_PID", QByteArray::number(wfPID));
}
