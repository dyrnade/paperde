/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of PaperDesktop.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QtWidgets>
#include <QtGui>
#include <QtCore>

class PaperBG : public QWidget {
	Q_OBJECT

public:
	PaperBG(QScreen *scr, QString img = QString());
	~PaperBG();

private:
	/** */
	void cacheImage();

	/** Is the cached image fresh? */
	bool isCacheFresh();

	void prepareBackgroundImage();
	void reloadSettings(QString, QVariant);

	QString bgImagePath;
	QString cachedImagePath;
	QImage desktopImage;

	int wallpaperPos = 0;
	QPoint mPicOffset;
	void resizeDesktop();

	QScreen *mScreen;

public Q_SLOTS:
	void handleMessages(QString, int);

protected:
	void paintEvent(QPaintEvent *pEvent);
};
