/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of PaperDesktop.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include "global.h"
#include "paperbg.h"

#include <paperde/papersettings.h>
#include <paper-config.h>

#include <cprime/systemxdg.h>

// Wayland Headers
#include <wayqt/WayQtUtils.hpp>

PaperBG::PaperBG(QScreen *scr, QString imageLoc) : QWidget()
{
	mScreen = scr;

	/* DesQ Shell Settings */
	connect(shellSett, &Paper::Settings::settingChanged, this, &PaperBG::reloadSettings);

	/* We need our desktop to fill the whole physical screen */
	setFixedSize(mScreen->size());
	connect(mScreen, &QScreen::geometryChanged, this, &PaperBG::resizeDesktop);

	/* If the display manager is X11, we need to let it know that we are a desktop */
	setAttribute(Qt::WA_X11NetWmWindowTypeDesktop);

	/* Title */
	setWindowTitle("Paper Background");

	/* No frame, and stay at bottom */
	Qt::WindowFlags wFlags = Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint | Qt::BypassWindowManagerHint;

	/* Set the windowFlags */
	setWindowFlags(wFlags);

	/* Background image */
	bgImagePath = imageLoc;

	/* Background position */
	wallpaperPos = shellSett->value("Personalization/WallpaperPosition");

	/* Prepare the background */
	prepareBackgroundImage();
}


PaperBG::~PaperBG()
{
	delete shellSett;
}


void PaperBG::cacheImage()
{
	/**
	  * We'll be saving the image as ~/.cache/DesQ/background.xxx
	  * Additionally, we'll write an info file that contains the image path,QFileInfo info( bgImagePath );
	  * wallpaper position, hash, etc. This info file will be used
	  * by isCacheFresh().
	  */

	QDir cache(CPrime::SystemXdg::userDir(CPrime::SystemXdg::XDG_CACHE_HOME) + "/paperde/");

	QFile imgFile(bgImagePath);

	imgFile.open(QFile::ReadOnly);

	QCryptographicHash hash(QCryptographicHash::Sha512);

	hash.addData(imgFile.readAll());
	imgFile.close();

	QSettings sett(cache.filePath("background.ini"), QSettings::IniFormat);

	sett.setValue("File", bgImagePath);
	sett.setValue("Position", wallpaperPos);
	sett.setValue("Hash", hash.result());

	// QString sfx( QFileInfo( bgImagePath ).suffix() );
	desktopImage.save(cache.filePath("background.jpg"));
}


bool PaperBG::isCacheFresh()
{
	QDir      cache(CPrime::SystemXdg::userDir(CPrime::SystemXdg::XDG_CACHE_HOME) + "/paperde/");
	QSettings sett(cache.filePath("background.ini"), QSettings::IniFormat);

	if (not QFile::exists(cache.filePath("background.jpg")))
	{
		return false;
	}

	if (sett.value("File").toString() != bgImagePath)
	{
		return false;
	}

	if (sett.value("Position") != wallpaperPos)
	{
		return false;
	}

	QFile imgFile(bgImagePath);

	imgFile.open(QFile::ReadOnly);

	QCryptographicHash hash(QCryptographicHash::Sha512);

	hash.addData(imgFile.readAll());
	imgFile.close();

	if (sett.value("Hash").toByteArray() != hash.result())
	{
		return false;
	}

	return true;
}


void PaperBG::prepareBackgroundImage()
{
	/* If the BG path was not specified by the user, use saved path */
	if (bgImagePath.isEmpty())
	{
		bgImagePath = ( QString )shellSett->value("Personalization/Background");
	}

	/** If the user does not want a background image */
	if (bgImagePath == "None")
	{
		return;
	}

	/** User has specified a solid color? */
	if (bgImagePath.startsWith("#"))
	{
		QColor clr(bgImagePath);
		desktopImage.fill(clr);

		repaint();

		return;
	}

	/* If the specified path/saved path does not exist, use the default value */
	if (not QFile::exists(bgImagePath))
	{
		bgImagePath = SharePath "background/default.svg";
	}

	if (isCacheFresh())
	{
		desktopImage = QImage(cachedImagePath);

		if (desktopImage.size() == mScreen->size())
		{
			return;
		}

		else
		{
			qDebug() << desktopImage.size() << mScreen->size();
		}
	}

	QSize drawSize = mScreen->geometry().size();

	QImageReader ir(bgImagePath);
	QSize        pixSize = ir.size();

	mPicOffset = QPoint();

	switch (wallpaperPos)
	{
	// Centered: Show the full wallpaper at the center
	case 0:
		pixSize    = pixSize.scaled(drawSize, Qt::KeepAspectRatio);
		mPicOffset = { abs(pixSize.width() - drawSize.width()) / 2, abs(pixSize.height() - drawSize.height()) / 2 };

		break;

	// Stretched: Ignore aspect ratio
	case 1:
		pixSize = pixSize.scaled(drawSize, Qt::IgnoreAspectRatio);
		break;

	// Scaled: Qt::KeepAspectRatioByExpanding
	case 2:
		pixSize = pixSize.scaled(drawSize, Qt::KeepAspectRatioByExpanding);
		break;
	}

	ir.setScaledSize(pixSize);

	/* Currently, the default setting: Scaled and cropped */
	QImage tmpImg = ir.read();

	desktopImage = tmpImg.copy(QRect(QPoint(0, 0), mScreen->size()));

	/** Trigger a repaint */
	repaint();

	/* Cache the scaled image so that it can be used later on */
	cacheImage();
}


void PaperBG::reloadSettings(QString key, QVariant)
{
	if (key == "Personalization/Background")
	{
		bgImagePath = ( QString )shellSett->value("Personalization/Background");
	}

	else if (key == "Personalization/WallpaperPosition")
	{
		wallpaperPos = shellSett->value("Personalization/WallpaperPosition");
	}

	prepareBackgroundImage();
}


void PaperBG::resizeDesktop()
{
	qDebug() << "Screen resized";
	qDebug() << mScreen->size();

	/* Reset the background */
	prepareBackgroundImage();

	/* We need our desktop to fill the whole physical screen */
	setFixedSize(mScreen->size());
}


void PaperBG::handleMessages(QString message, int /* fd */)
{
	if (message.startsWith("set-background "))
	{
		bgImagePath = message.mid(15);
		shellSett->setValue("Background", bgImagePath);
		prepareBackgroundImage();
	}

	if (message.startsWith("background "))
	{
		bgImagePath = message.mid(11);
		prepareBackgroundImage();
	}
}


void PaperBG::paintEvent(QPaintEvent *pEvent)
{
	/** If we have a valid desktop image */
	if (not desktopImage.isNull())
	{
		QPainter painter(this);

		painter.setPen(Qt::NoPen);
		painter.setPen(Qt::NoBrush);

		painter.setRenderHints(QPainter::Antialiasing);

		painter.drawImage(geometry(), desktopImage);

		painter.end();
	}

	/** Done with the painting */
	pEvent->accept();
}
