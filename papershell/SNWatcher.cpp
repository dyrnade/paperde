/**
  * Copyright (c) 2022
  *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
  *
  * This is a very simple status notifier watcher daemon.
  **/

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <QtCore>
#include <QDebug>
#include <QDBusConnectionInterface>

#include <DFL/DF6/StatusNotifierWatcher.hpp>

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);

	app.setOrganizationName("CuboCore");
	app.setApplicationName("papersnwatcher");
	app.setApplicationVersion("v" PROJECT_VERSION);

	QCommandLineParser parser;

	parser.addHelpOption();         // Help
	parser.addVersionOption();      // Version

	parser.addOption({ { "d", "daemon" }, "Run this application as a daemon", "" });
	parser.addOption({ "no-daemon", "Don't run this application as a daemon", "" });

	parser.process(app);

	/*
	  *
	  * The reference used for writing this code into a daemon.
	  * http://www.netzmafia.de/skripten/unix/linux-daemon-howto.html
	  *
	  */

	if (not parser.isSet("no-daemon"))
	{
		pid_t pid = fork();

		if (pid < 0)
		{
			// Exit the program
			exit(EXIT_FAILURE);
		}

		if (pid > 0)
		{
			// Exit the parent
			exit(EXIT_SUCCESS);
		}

		/* Change the file mode mask */
		umask(0);

		/* Create a new SID for the child process */
		pid_t sid = setsid();

		if (sid < 0)
		{
			// TODO: Write to log file
			exit(EXIT_FAILURE);
		}

		/* Change to home folder */
		if (chdir(QDir::homePath().toUtf8().data()) < 0)
		{
			// TODO: Write to error log
			exit(EXIT_FAILURE);
		}

		/* Close the std file descriptors */
		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);
	}

	/* Our actual code starts here */

	DFL::StatusNotifierWatcher *sniw = new DFL::StatusNotifierWatcher();

	/* Some one else has taken this service!! Grrr... */
	if (not sniw->isServiceRunning())
	{
		qCritical() << "Unable to start the service 'org.kde.StatusNotifierWatcher'.";
		qCritical() << "Please ensure that another instance of this app is not running elsewhere.";
		return EXIT_FAILURE;
	}

	/* Something went wrong... */
	else if (not sniw->isObjectRegistered())
	{
		qCritical() << "Unable to register the object '/StatusNotifierWatcher'.";
		qCritical() << "Please ensure that another instance of this app is not running elsewhere.";
		return EXIT_FAILURE;
	}

	/* All is well. */
	else
	{
		qInfo() << "Service org.kde.StatusNotifierWatcher running...";
		qInfo() << "Object /StatusNotifierWatcher registered...";
		qInfo() << "Ready for incoming connections...";
	}

	return app.exec();
}
