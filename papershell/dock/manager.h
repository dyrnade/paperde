/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QtCore>

#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/WayQtUtils.hpp>

#include <DFL/DF6/IpcClient.hpp>

#include "dockbar.h"

namespace Paper {
namespace Dock {
class Manager;
}
}

class pageapps;
class QVariantAnimation;

/**
  * @class Paper::Dock::Manager
  * This class will handle the creation, showing, hiding and destruction
  * of Paper::Dock::UI on one or all screens.
  * Additionally, it will also handle the CLI requests, if any
  */
class Paper::Dock::Manager : public QObject {
	Q_OBJECT;

public:
	Manager();
	~Manager();

	/**
	  * Load the user settings.
	  */
	void loadSettings();

	/**
	  * Handle the requests from other instance. To reply to queries,
	  * write suitable data to the file descriptor @fd.
	  */
	Q_SLOT void handleMessages(QString msg, int fd);

	/**
	  * Start an instance of Paper::Dock::UI on screen @scrn
	  */
	void createInstance(QScreen *scrn);

	/**
	  * Stop an instance of Paper::Dock::UI on screen named @opName
	  */
	void destroyInstance(QString opName);

	/**
	  * Raise an existing instance of Paper::Dock::UI
	  * which is open on screen named @opName.
	  * NOTE: To be used when Paper::Dock::UI is shown on desktop.
	  */
	void raiseInstance(QString opName);

	/**
	  * Lower an existing instance of Paper::Dock::UI
	  * which is open on screen named @opName.
	  * NOTE: To be used when Paper::Dock::UI is shown on desktop.
	  */
	void lowerInstance(QString opName);

	/**
	  * Show a layer surface on the output named @opName
	  * If no instance of Paper::Dock::UI exists on this output
	  * nothing will be done.
	  * NOTE: This will automatically show the widget as top layer.
	  */
	void showLayerSurface(QString opName);

	/**
	  * Hide a layer surface on the output named @opName
	  */
	void hideLayerSurface(QString opName);

private:
	QHash<QString, Paper::Dock::UI *> mInstances;
	QHash<QString, bool> mInstanceStates;

	QHash<QString, WQt::LayerSurface *> mSurfaces;
	QHash<QString, WQt::LayerShell::LayerType> mSurfaceStates;

	/**
	  * Always shown in the background
	  */
	bool mBackgroundMode = true;

	/**
	  * IPC Connections to Paper Components
	  */
	DFL::IPC::Client *widgets = nullptr;
	DFL::IPC::Client *menu    = nullptr;

	bool menuVisible    = false;
	bool widgetsVisible = false;

	/**
	  * Return an animator which will animate a layer surface when started.
	  * @surf - Layer surface to be moved
	  * @pw   - Instance of paperwidget (used to calculate the margin positions)
	  * @show - If true, the widget is being shown, otherwise hidden
	  */
	QVariantAnimation *createAnimator(WQt::LayerSurface *surf, Paper::Dock::UI *ui, bool show);

	/**
	  * Show/hide the Application Menu.
	  * If the menu is not initialized yet, we will initialize and then show it.
	  * The PaperDock instance on the output named @opName will be made opaque.
	  */
	void toggleMenu(QString opName);

	/**
	  * Show/hide the Paper Widgets.
	  * The PaperDock instance on the output named @opName will be made opaque.
	  */
	void toggleWidgets(QString opName);
};
