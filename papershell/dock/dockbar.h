/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QWidget>
#include <QToolButton>
#include <QBasicTimer>

namespace Paper {
namespace Dock {
class UI;
class TaskBar;
}
}

class Paper::Dock::UI : public QWidget {
	Q_OBJECT;

public:
	UI(QString opName);
	~UI();

	void setShownAsBackground(bool);
	void makeOpaque(bool);

	/**
	  * Mark the widgets
	  * When PaperWidgets are shown, highligh the button
	  */
	void markWidgets(bool);


	/**
	  * Mark the menu
	  * When PaperWidgets are shown, highligh the button
	  */
	void markMenu(bool);

private:
	bool mBackgroundMode = false;
	bool mRoundedCorners = false;

	QToolButton *menuBtn;
	QToolButton *widgetsBtn;
	QToolButton *logoutBtn;
	QBasicTimer timer;

	Paper::Dock::TaskBar *tasks;

	/**
	  * This is used when calling the paperwidgets:
	  * paperwidgets --toggle --output @mOutputName
	  */
	QString mOutputName;

	/**
	  * Opaque mode
	  * When applications are shown, or PaperWidgets are shown,
	  * the dock should become opaque.
	  */
	bool mOpaqueMode = false;

	/**
	  * Fix the size of the dock
	  */
	void prepareDock();

	void buildUI();

	void loadFixedTasks();

	/**
	  * Clock tick. We update the time every second.
	  */
	void updateTime();

	/**
	  * To handle the timer.
	  */
	void timerEvent(QTimerEvent *event) override;

protected:
	void paintEvent(QPaintEvent *);

Q_SIGNALS:
	void toggleMenu(QString);
	void toggleWidgets(QString);
};
