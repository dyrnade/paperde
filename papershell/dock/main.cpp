/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include <QMessageBox>
#include <QScreen>
#include <QDebug>
#include <QTime>

#include <signal.h>

#include <cprime/capplication.h>
#include <cprime/systemxdg.h>
#include <paperde/paperlog.h>

#include <DFL/DF6/Application.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>

#include "global.h"
#include "manager.h"

#include <paper-config.h>

Paper::Settings *paperShellSM = nullptr;
WQt::Registry   *wlRegistry   = nullptr;

int main(int argc, char *argv[])
{
	/* Disable window manager hints for this window */
	qputenv("QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral("1"));

	QDir cache(CPrime::SystemXdg::userDir(CPrime::SystemXdg::XDG_CACHE_HOME));

	Paper::log = fopen(cache.filePath("paperde/PaperDock.log").toLocal8Bit().data(), "a");

	qInstallMessageHandler(Paper::Logger);

	qDebug() << "------------------------------------------------------------------------";
	qDebug() << "Paper Dock started at" << QDateTime::currentDateTime().toString("yyyyMMddThhmmss").toUtf8().constData();
	qDebug() << "------------------------------------------------------------------------";

	QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	DFL::Application app(argc, argv);

	// Set application info
	app.setOrganizationName("CuboCore");
	app.setApplicationName(APPNAME);
	app.setApplicationVersion(QStringLiteral(PAPER_VERSION_STR));

	app.interceptSignal(SIGSEGV, true);

	QCommandLineParser parser;

	parser.addHelpOption();         // Help
	parser.addVersionOption();      // Version

	parser.addOption({ { "r", "raise" }, "Raise the PaperDock isntance to the top" });
	parser.addOption({ { "l", "lower" }, "Lower the PaperDock isntance to the bottom" });
	parser.addOption({ { "t", "toggle" }, "Show/hide the PaperDock isntance" });
	parser.addOption({ { "o", "output" }, "Name of the output on which raise/lower/toggle is done.", "output" });

	parser.process(app);

	if (app.lockApplication())
	{
		paperShellSM = new Paper::Settings("papershell");
		wlRegistry   = new WQt::Registry(WQt::Wayland::display());
		wlRegistry->setup();

		Paper::Dock::Manager *pdMgr = new Paper::Dock::Manager();

		QObject::connect(&app, &DFL::Application::messageFromClient, pdMgr, &Paper::Dock::Manager::handleMessages);

		QObject::connect(&app, &QApplication::screenAdded, [pdMgr] ( QScreen *screen ) {
			pdMgr->createInstance(screen);
		});

		return app.exec();
	}

	else
	{
		if (parser.isSet("raise"))
		{
			if (not parser.isSet("output"))
			{
				qCritical() << "Please specify the output on which PaperDock instance is to be raised.";
				return 1;
			}

			app.messageServer("raise\n" + parser.value("output"));
		}

		else if (parser.isSet("lower"))
		{
			if (not parser.isSet("output"))
			{
				qCritical() << "Please specify the output on which PaperDock instance is to be lowered.";
				return 1;
			}

			app.messageServer("lower\n" + parser.value("output"));
		}

		else if (parser.isSet("toggle"))
		{
			if (not parser.isSet("output"))
			{
				qCritical() << "Please specify the output on which PaperDock instance is to be toggled.";
				return 1;
			}

			app.messageServer("toggle\n" + parser.value("output"));
		}

		else
		{
			printf("Paper Dock v" PAPER_VERSION_STR "\n");
			parser.showHelp();
		}

		return 0;
	}

	return 0;
}
