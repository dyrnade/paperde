/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include "global.h"
#include "tasks.h"

#include <filesystem>

#include <QtWidgets>
#include <wayqt/Registry.hpp>
#include <wayqt/WindowManager.hpp>


const static QString taskCSS = QString(
	"QToolButton {"
	"   border: none;"
	"   border-radius: 0px;"
	"   background-color: transparent;"
	"   border-left: 2px solid %1;"
	"}"
	"QToolButton:hover {"
	"   background-color: transparent;"
	"   border-left: 2px solid palette(Highlight);"
	"}"
	"QToolButton:pressed {"
	"   margin-left: 1px;"
	"   margin-top: 1px;"
	"}"
	);

static inline QIcon getIconForAppId(QString mAppId)
{
	/* Hack for gnome terminal */
	if (mAppId == "gnome-terminal-server")
	{
		return QIcon::fromTheme("org.gnome.Terminal");
	}

	if (mAppId.isEmpty())
	{
		return QIcon::fromTheme("application-x-executable");
	}

	if (mAppId == "Unknown")
	{
		return QIcon::fromTheme("application-x-executable");
	}

	QStringList appDirs = {
		QDir::home().filePath(".local/share/applications/"),
		"/usr/local/share/applications/",
		"/usr/share/applications/",
	};

	QString iconName;
	bool    found = false;

	/** Assume mAppId == desktop-file-name */
	for ( QString path: appDirs )
	{
		if (QFile::exists(path + mAppId + ".desktop"))
		{
			QSettings desktop(path + mAppId + ".desktop", QSettings::IniFormat);
			iconName = desktop.value("Desktop Entry/Icon", "application-x-executable").toString();

			if (not iconName.isEmpty())
			{
				if (not QIcon::hasThemeIcon(iconName))
				{
					QString png = "/usr/share/pixmaps/" + iconName + ".png";
					QString xpm = "/usr/share/pixmaps/" + iconName + ".xpm";
					QString svg = "/usr/share/pixmaps/" + iconName + ".svg";

					if (QFile::exists(png))
					{
						iconName = png;
						found    = true;
					}

					else if (QFile::exists(svg))
					{
						iconName = svg;
						found    = true;
					}

					else if (QFile::exists(xpm))
					{
						iconName = xpm;
						found    = true;
					}

					else
					{
						found = false;
					}
				}

				break;
			}
		}

		else if (QFile::exists(path + mAppId.toLower() + ".desktop"))
		{
			QSettings desktop(path + mAppId.toLower() + ".desktop", QSettings::IniFormat);
			iconName = desktop.value("Desktop Entry/Icon", "application-x-executable").toString();

			if (not iconName.isEmpty())
			{
				if (not QIcon::hasThemeIcon(iconName))
				{
					QString png = "/usr/share/pixmaps/" + iconName + ".png";
					QString xpm = "/usr/share/pixmaps/" + iconName + ".xpm";
					QString svg = "/usr/share/pixmaps/" + iconName + ".svg";

					if (QFile::exists(png))
					{
						iconName = png;
						found    = true;
					}

					else if (QFile::exists(svg))
					{
						iconName = svg;
						found    = true;
					}

					else if (QFile::exists(xpm))
					{
						iconName = xpm;
						found    = true;
					}

					else
					{
						found = false;
					}
				}

				break;
			}
		}
	}

	/** Check if we can get some icon named mAppId.[png|svg|xpm] */
	if (not found)
	{
		if (QIcon::hasThemeIcon(mAppId))
		{
			iconName = mAppId;
			found    = true;
		}

		else if (QIcon::hasThemeIcon(mAppId.toLower()))
		{
			iconName = mAppId.toLower();
			found    = true;
		}

		else
		{
			QString png = "/usr/share/pixmaps/" + mAppId + ".png";
			QString xpm = "/usr/share/pixmaps/" + mAppId + ".xpm";
			QString svg = "/usr/share/pixmaps/" + mAppId + ".svg";

			if (QFile::exists(png))
			{
				iconName = png;
				found    = true;
			}

			else if (QFile::exists(svg))
			{
				iconName = svg;
				found    = true;
			}

			else if (QFile::exists(xpm))
			{
				iconName = xpm;
				found    = true;
			}

			else
			{
				found = false;
			}
		}
	}

	if (not found)
	{
		/* Check all desktop files for */
		for ( QString path: appDirs )
		{
			QStringList desktops = QDir(path).entryList({ "*.desktop" });
			for ( QString dskf: desktops )
			{
				QSettings desktop(path + dskf, QSettings::IniFormat);

				QString exec = desktop.value("Desktop Entry/Exec", "abcd1234/-").toString();
				QString name = desktop.value("Desktop Entry/Name", "abcd1234/-").toString();
				QString cls  = desktop.value("Desktop Entry/StartupWMClass", "abcd1234/-").toString();

				QString execPath(std::filesystem::path(exec.toStdString()).filename().c_str());

				if (mAppId.compare(execPath, Qt::CaseInsensitive) == 0)
				{
					iconName = desktop.value("Desktop Entry/Icon", "application-x-executable").toString();
					break;
				}

				else if (mAppId.compare(name, Qt::CaseInsensitive) == 0)
				{
					iconName = desktop.value("Desktop Entry/Icon", "application-x-executable").toString();
					break;
				}

				else if (mAppId.compare(cls, Qt::CaseInsensitive) == 0)
				{
					iconName = desktop.value("Desktop Entry/Icon", "application-x-executable").toString();
					break;
				}
			}
		}
	}

	qDebug() << mAppId << iconName;

	if (iconName.startsWith("/"))
	{
		return QIcon(iconName);
	}

	else if (QIcon::hasThemeIcon(iconName))
	{
		return QIcon::fromTheme(iconName);
	}

	return QIcon::fromTheme("application-x-executable");
}


Paper::Dock::TaskBar::TaskBar(QWidget *parent) : QWidget(parent)
{
	winMgr = wlRegistry->windowManager();
	connect(winMgr, &WQt::WindowManager::newTopLevelHandle, this, &Paper::Dock::TaskBar::addWindow);

	winMgr->setup();

	tasksLyt = new QVBoxLayout();
	tasksLyt->setContentsMargins(QMargins());
	tasksLyt->setSpacing(10);

	setLayout(tasksLyt);
}


void Paper::Dock::TaskBar::addWindow(WQt::WindowHandle *tl)
{
	QToolButton *btn = new QToolButton();

	btn->setFixedSize(40, 40);
	btn->setIconSize(QSize(36, 36));
	btn->setIcon(getIconForAppId(tl->appId()));

	connect(btn, &QToolButton::clicked, [ = ] () {
		if (tl->isActivated())
		{
			tl->setMinimized();
		}
		else
		{
			if (tl->isMinimized())
			{
				tl->unsetMinimized();
			}
			tl->activate(wlRegistry->waylandSeat());
		}
	});

	tasksLyt->addWidget(btn);

	mTlTaskHash[tl] = btn;

	connect(tl, &WQt::WindowHandle::appIdChanged, [ = ]() {
		updateIcon(tl);
	});

	connect(tl, &WQt::WindowHandle::stateChanged, [ = ]() {
		updateState();
	});

	connect(tl, &WQt::WindowHandle::closed, [ = ]() {
		removeWindow(tl);
	});

	tl->setup();

	setFixedHeight(mTlTaskHash.count() * 40 + (mTlTaskHash.count() - 1) * tasksLyt->spacing());
}


void Paper::Dock::TaskBar::removeWindow(WQt::WindowHandle *tl)
{
	QToolButton *btn = mTlTaskHash.take(tl);

	tasksLyt->removeWidget(btn);

	delete tl;
	delete btn;

	setFixedHeight(mTlTaskHash.count() * 40 + (mTlTaskHash.count() - 1) * tasksLyt->spacing());
}


void Paper::Dock::TaskBar::updateIcon(WQt::WindowHandle *tl)
{
	QString appID = tl->appId().split(" ", Qt::SkipEmptyParts).value(0);

	mTlTaskHash[tl]->setIcon(getIconForAppId(appID));
}


void Paper::Dock::TaskBar::updateState()
{
	for ( QToolButton *btn: mTlTaskHash.values())
	{
		btn->setStyleSheet(taskCSS.arg("palette(Highlight)"));
	}

	for ( WQt::WindowHandle *tl: mTlTaskHash.keys())
	{
		if (tl->isActivated())
		{
			mTlTaskHash[tl]->setStyleSheet(taskCSS.arg("#FF8C00"));
		}

		if (tl->isMinimized())
		{
			mTlTaskHash[tl]->setStyleSheet(taskCSS.arg("#008B8B"));
		}
	}
}
