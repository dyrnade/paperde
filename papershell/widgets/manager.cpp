/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include "UI/paperwidgets.h"
#include "manager.h"
#include "global.h"

#include <QSequentialAnimationGroup>
#include <QVariantAnimation>
#include <QEasingCurve>
#include <QScreen>

#include <DFL/DF6/Application.hpp>

Paper::Widgets::Manager::Manager()
{
	for ( QScreen *scrn: qApp->screens())
	{
		createInstance(scrn);

		if (mBackgroundMode)
		{
			QVariantAnimation *showAnim = createAnimator(mSurfaces[scrn->name()], mInstances[scrn->name()], true);
			showAnim->start();
			mInstanceStates[scrn->name()] = true;
		}
	}
}


Paper::Widgets::Manager::~Manager()
{
}


void Paper::Widgets::Manager::handleMessages(QString msg, int fd)
{
	if (msg == "reload-widgets")
	{
		for ( paperwidgets *pw: mInstances.values())
		{
			pw->reloadPlugins();
		}

		qApp->messageClient("ack", fd);
	}

	else if (msg.startsWith("raise"))
	{
		QStringList parts = msg.split("\n");
		raiseInstance(parts[1]);

		qApp->messageClient("ack", fd);
	}

	else if (msg.startsWith("lower"))
	{
		QStringList parts = msg.split("\n");
		lowerInstance(parts[1]);

		qApp->messageClient("ack", fd);
	}

	else if (msg.startsWith("toggle"))
	{
		QStringList parts = msg.split("\n");
		if (mInstanceStates[parts[1]])
		{
			hideLayerSurface(parts[1]);
			qApp->messageClient("hidden", fd);
		}

		else
		{
			showLayerSurface(parts[1]);
			qApp->messageClient("shown", fd);
		}
	}

	else
	{
		qWarning() << "Unhandled request:" << msg;
		qApp->messageClient("Unhandled request", fd);
	}
}


void Paper::Widgets::Manager::createInstance(QScreen *scrn)
{
	paperwidgets *home = new paperwidgets;

	connect(home, &paperwidgets::hideWidgets, [ = ]() {
		hideLayerSurface(scrn->name());
	});

	home->show();

	if (WQt::Utils::isWayland())
	{
		/** wl_output corresponding to @screen */
		wl_output *output = WQt::Utils::wlOutputFromQScreen(scrn);

		WQt::LayerSurface *cls = wlRegistry->layerShell()->getLayerSurface(
			home->windowHandle(),             // Window Handle
			output,                           // wl_output object - for multi-monitor support
			WQt::LayerShell::Bottom,          // Background layer
			"widgets"                         // Dummy namespace
			);

		/** Anchor everywhere */
		cls->setAnchors(
			WQt::LayerSurface::Top |
			WQt::LayerSurface::Bottom |
			WQt::LayerSurface::Left
			);

		/** Size - when */
		cls->setSurfaceSize(home->size());

		/** Nothing should move this */
		cls->setExclusiveZone(-1);

		/** No keyboard interaction */
		cls->setKeyboardInteractivity(WQt::LayerSurface::OnDemand);

		/** Default margins: hide the surface */
		cls->setMargins(QMargins(-home->width(), 0, 0, 0));

		/** Apply the changes */
		cls->apply();

		mInstances[scrn->name()]      = home;
		mSurfaces[scrn->name()]       = cls;
		mSurfaceStates[scrn->name()]  = WQt::LayerShell::Bottom;
		mInstanceStates[scrn->name()] = false;
	}

	else
	{
		home->close();
		delete home;
	}
}


void Paper::Widgets::Manager::destroyInstance(QString opName)
{
	if (mSurfaces.contains(opName))
	{
		WQt::LayerSurface *surf = mSurfaces.take(opName);
		delete surf;
	}

	if (mSurfaceStates.contains(opName))
	{
		mSurfaceStates.remove(opName);
	}

	if (mInstances.contains(opName))
	{
		paperwidgets *w = mInstances.take(opName);
		w->close();
		delete w;
	}

	if (mInstanceStates.contains(opName))
	{
		mInstanceStates.remove(opName);
	}
}


void Paper::Widgets::Manager::raiseInstance(QString opName)
{
	if (not mSurfaces.contains(opName))
	{
		return;
	}

	if (mSurfaceStates[opName] == WQt::LayerShell::Top)
	{
		return;
	}

	QVariantAnimation *hideAnim = createAnimator(mSurfaces[opName], mInstances[opName], false);
	QVariantAnimation *showAnim = createAnimator(mSurfaces[opName], mInstances[opName], true);

	connect(hideAnim, &QVariantAnimation::finished, [ = ] () {
		mSurfaceStates[opName] = WQt::LayerShell::Top;

		mSurfaces[opName]->setLayer(WQt::LayerShell::Top);
		mSurfaces[opName]->apply();

		mInstances[opName]->setShownAsBackground(false);
	});

	QSequentialAnimationGroup *grp = new QSequentialAnimationGroup();
	grp->addAnimation(hideAnim);
	grp->addAnimation(showAnim);

	grp->start();
	mInstanceStates[opName] = true;
}


void Paper::Widgets::Manager::lowerInstance(QString opName)
{
	if (not mSurfaces.contains(opName))
	{
		return;
	}

	if (mSurfaceStates[opName] == WQt::LayerShell::Bottom)
	{
		return;
	}

	QVariantAnimation *hideAnim = createAnimator(mSurfaces[opName], mInstances[opName], false);
	QVariantAnimation *showAnim = createAnimator(mSurfaces[opName], mInstances[opName], true);

	connect(hideAnim, &QVariantAnimation::finished, [ = ] () {
		mSurfaceStates[opName] = WQt::LayerShell::Bottom;

		mSurfaces[opName]->setLayer(WQt::LayerShell::Bottom);
		mSurfaces[opName]->apply();

		mInstances[opName]->setShownAsBackground(true);
	});

	QSequentialAnimationGroup *grp = new QSequentialAnimationGroup();
	grp->addAnimation(hideAnim);
	grp->addAnimation(showAnim);

	grp->start();
	mInstanceStates[opName] = true;
}


void Paper::Widgets::Manager::showLayerSurface(QString opName)
{
	if (not mInstances.contains(opName))
	{
		return;
	}

	mSurfaceStates[opName] = WQt::LayerShell::Top;

	mSurfaces[opName]->setLayer(WQt::LayerShell::Top);
	mSurfaces[opName]->apply();

	mInstances[opName]->setShownAsBackground(false);

	QVariantAnimation *showAnim = createAnimator(mSurfaces[opName], mInstances[opName], true);
	showAnim->start();

	mInstanceStates[opName] = true;
}


void Paper::Widgets::Manager::hideLayerSurface(QString opName)
{
	QVariantAnimation *hideAnim = createAnimator(mSurfaces[opName], mInstances[opName], false);

	hideAnim->start();

	mInstanceStates[opName] = false;
}


QVariantAnimation * Paper::Widgets::Manager::createAnimator(WQt::LayerSurface *surf, paperwidgets *pw, bool show)
{
	QVariantAnimation *anim = new QVariantAnimation();

	anim->setDuration(500);

	if (show)
	{
		anim->setStartValue(-pw->width());
		anim->setEndValue(40);
		anim->setEasingCurve(QEasingCurve(QEasingCurve::OutCubic));
	}

	else
	{
		anim->setStartValue(40);
		anim->setEndValue(-pw->width());
		anim->setEasingCurve(QEasingCurve(QEasingCurve::InOutQuart));
	}

	connect(anim, &QVariantAnimation::valueChanged, [ = ]( QVariant val ) {
		surf->setMargins(QMargins(val.toInt(), 0, 0, 0));
		surf->apply();

		qApp->processEvents();
	});

	return anim;
}
