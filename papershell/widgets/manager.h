/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QtCore>

#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/WayQtUtils.hpp>

#include "UI/paperwidgets.h"

namespace Paper {
namespace Widgets {
class Manager;
}
}

class QVariantAnimation;

/**
  * @class Paper::Widgets::Manager
  * This class will handle the creation, showing, hiding and destruction
  * of paperwidgets UI on one or all screens.
  * Additionally, it will also handle the CLI requests
  */
class Paper::Widgets::Manager : public QObject {
	Q_OBJECT;

public:
	Manager();
	~Manager();

	/**
	  * Load the user settings.
	  */
	void loadSettings();

	/**
	  * Handle the requests from other instance. To reply to queries,
	  * write suitable data to the file descriptor @fd.
	  */
	Q_SLOT void handleMessages(QString msg, int fd);

	/**
	  * Start an instance of paperwidgets on screen @scrn
	  */
	void createInstance(QScreen *scrn);

	/**
	  * Stop an instance of paperwidgets on screen named @opName
	  */
	void destroyInstance(QString opName);

	/**
	  * Raise an existing instance of paperwidgets
	  * which is open on screen named @opName.
	  * NOTE: To be used when paperwidgets is shown on desktop.
	  */
	void raiseInstance(QString opName);

	/**
	  * Lower an existing instance of paperwidgets
	  * which is open on screen named @opName.
	  * NOTE: To be used when paperwidgets is shown on desktop.
	  */
	void lowerInstance(QString opName);

	/**
	  * Show a layer surface on the output named @opName
	  * If no instance of paperwidgets exists on this output
	  * nothing will be done.
	  * NOTE: This will automatically show the widget as top layer.
	  */
	void showLayerSurface(QString opName);

	/**
	  * Hide a layer surface on the output named @opName
	  */
	void hideLayerSurface(QString opName);

private:
	QHash<QString, paperwidgets *> mInstances;
	QHash<QString, bool> mInstanceStates;

	QHash<QString, WQt::LayerSurface *> mSurfaces;
	QHash<QString, WQt::LayerShell::LayerType> mSurfaceStates;

	bool mBackgroundMode = false;

	/**
	  * Return an animator which will animate a layer surface when started.
	  * @surf - Layer surface to be moved
	  * @pw   - Instance of paperwidget (used to calculate the margin positions)
	  * @show - If true, the widget is being shown, otherwise hidden
	  */
	QVariantAnimation *createAnimator(WQt::LayerSurface *surf, paperwidgets *pw, bool show);
};
