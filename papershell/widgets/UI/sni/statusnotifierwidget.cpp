/* BEGIN_COMMON_COPYRIGHT_HEADER
 * (c)LGPL2+
 *
 * LXQt - a lightweight, Qt based, desktop toolset
 * https://lxqt.org
 *
 * Copyright: 2015 LXQt team
 * Authors:
 *  Balázs Béla <balazsbela[at]gmail.com>
 *  Paulo Lieuthier <paulolieuthier@gmail.com>
 *
 * This program or library is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 *
 * END_COMMON_COPYRIGHT_HEADER */

#include <QApplication>
#include <QDebug>

#include <paperde/dynamiclayout.h>
#include <DFL/DF6/StatusNotifierWatcher.hpp>

#include "statusnotifierwidget.h"
#include "statusnotifierbutton.h"
#include "global.h"

#define WATCHER_SERVICE    "org.kde.StatusNotifierWatcher"
#define WATCHER_OBJECT     "/StatusNotifierWatcher"
#define WATCHER_PATH       "org.kde.StatusNotifierWatcher"

StatusNotifierWidget::StatusNotifierWidget(LayoutStyle lytStyle, QWidget *parent) : QGroupBox(parent)
{
	mLytStyle = lytStyle;

	/** Allow Groupbox margins onlu in Grid style layout */
	if (mLytStyle != Grid)
	{
		setContentsMargins(QMargins());
	}

	QString dbusName = QString("org.kde.StatusNotifierWidget-%1-%2").arg(QApplication::applicationPid()).arg(1);

	if (!QDBusConnection::sessionBus().registerService(dbusName))
	{
		qDebug() << QDBusConnection::sessionBus().lastError().message();
	}

	mWatcher = new QDBusInterface(WATCHER_SERVICE, WATCHER_OBJECT, WATCHER_PATH, QDBusConnection::sessionBus());
	mWatcher->call("RegisterStatusNotifierWidget", dbusName);

	QDBusConnection::sessionBus().connect(
		WATCHER_SERVICE,
		WATCHER_OBJECT,
		WATCHER_PATH,
		"StatusNotifierItemRegistered",
		"s",
		this,
		SLOT(itemAdded(QString))
		);

	QDBusConnection::sessionBus().connect(
		WATCHER_SERVICE,
		WATCHER_OBJECT,
		WATCHER_PATH,
		"StatusNotifierItemUnregistered",
		"s",
		this,
		SLOT(itemRemoved(QString))
		);

	sniLyt = new Paper::DynamicLayout();
	sniLyt->setAlignment(Qt::AlignLeft);
	sniLyt->setHorizontalSpacing(mItemSpacing);
	sniLyt->setVerticalSpacing(mItemSpacing);
	sniLyt->setContentsMargins(mMargins);

	QWidget *base = new QWidget(this);

	base->setObjectName("base");
	base->setLayout(sniLyt);

	QHBoxLayout *lyt = new QHBoxLayout();

	lyt->setContentsMargins(QMargins());
	lyt->addWidget(base);
	setLayout(lyt);

	QVariant registered = mWatcher->property("RegisteredStatusNotifierItems");

	for ( QString sni: registered.toStringList())
	{
		itemAdded(sni);
	}

	setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
	setAttribute(Qt::WA_TranslucentBackground);

	setButtonSize(mSize);
}


StatusNotifierWidget::~StatusNotifierWidget()
{
	delete mWatcher;
}


void StatusNotifierWidget::setButtonSize(int size)
{
	mSize = size;

	/* Resize the buttons */
	for ( QToolButton *btn: findChildren<QToolButton *>())
	{
		btn->setFixedSize(size, size);
		btn->setIconSize(QSize(size - 4, size - 4));
	}

	int width  = 0;
	int height = 0;

	QMargins cMargins(contentsMargins());

	switch (mLytStyle)
	{
	/* Width keeps increasing; height is set externally (ex: Horizontal Panel) */
	case Horizontal:
		width  = mServices.count() * size + mMargins.left() + mMargins.right() + mItemSpacing * (mServices.count() - 1);
		height = size + mMargins.top() + mMargins.bottom();

		setFixedWidth(width);
		break;

	/* Height keeps increasing; width is fixed externally (ex: Vertical Panel) */
	case Vertical:
		width  = size + mMargins.left() + mMargins.right();
		height = mServices.count() * size + mMargins.top() + mMargins.bottom() + mItemSpacing * (mServices.count() - 1);

		setFixedHeight(height);
		break;

	/* Width set externally; height can increase */
	case Grid:
	   {
		   int cell = size + mItemSpacing;
		   width = rect().width();
		   int availWidth = width - mMargins.left() - mMargins.right() - cMargins.top() - cMargins.bottom();
		   int cols       = floor(availWidth / cell);
		   int rows       = ceil(1.0 * mServices.count() / cols);

		   height  = rows * size + mMargins.top() + mMargins.bottom() + mItemSpacing * (rows - 1);
		   height += cMargins.top() + cMargins.bottom();

		   setFixedHeight(height);
		   break;
	   }
	}

	sniLyt->setGeometry(QRect(0, 0, width, height));
}


void StatusNotifierWidget::setLayoutStyle(LayoutStyle lytStyle)
{
	mLytStyle = lytStyle;
	setButtonSize(mSize);
}


void StatusNotifierWidget::setItemSpacing(int itmSpc)
{
	mItemSpacing = itmSpc;
	sniLyt->setHorizontalSpacing(itmSpc);
	sniLyt->setVerticalSpacing(itmSpc);
	setButtonSize(mSize);
}


void StatusNotifierWidget::setMargins(QMargins margins)
{
	mMargins = margins;
	sniLyt->setContentsMargins(margins);
	setButtonSize(mSize);
}


QSize StatusNotifierWidget::minimumSizeHint() const
{
	if (mLytStyle == Horizontal)
	{
		return QSize(sniLyt->count() * mSize, mSize);
	}

	else if (mLytStyle == Vertical)
	{
		return QSize(mSize, sniLyt->count() * mSize);
	}

	else
	{
		return sniLyt->sizeHint();
	}
}


void StatusNotifierWidget::itemAdded(QString serviceAndPath)
{
	if (mServices.contains(serviceAndPath))
	{
		return;
	}

	int                  slash   = serviceAndPath.indexOf('/');
	QString              serv    = serviceAndPath.left(slash);
	QString              path    = serviceAndPath.mid(slash);
	StatusNotifierButton *button = new StatusNotifierButton(serv, path, this);

	mServices.insert(serviceAndPath, button);
	sniLyt->addWidget(button);
	button->show();

	setButtonSize(mSize);
}


void StatusNotifierWidget::itemRemoved(QString serviceAndPath)
{
	StatusNotifierButton *button = mServices.value(serviceAndPath, nullptr);

	if (button)
	{
		button->deleteLater();
		sniLyt->removeWidget(button);

		mServices.remove(serviceAndPath);
	}

	setButtonSize(mSize);
}
