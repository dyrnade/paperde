/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QMap>
#include <QWidget>

class QGroupBox;
class QListWidgetItem;
class StatusNotifierWidget;

namespace Ui {
class paperwidgets;
}

class paperwidgets : public QWidget {
	Q_OBJECT

public:
	explicit paperwidgets();
	~paperwidgets();

	/**
	  * Used in painting the background
	  * If this is shown as background, then
	  * the transparency is higher (20%). If it's shown
	  * as active layer (i.e., on top), then the widget
	  * is 10% transparent;
	  */
	void setShownAsBackground(bool);

public Q_SLOTS:
	void reloadPlugins();

private:
	Ui::paperwidgets *ui;

	QStringList selectedPlugins;
	int uiMode;
	int existingWidgetsItemCount;
	QSize iconSize;

	StatusNotifierWidget *sniTray;
	int maxWidth = 270;

	bool mBackgroundMode = true;
	bool mRoundedCorners = false;

	/**
	  * Load the user settings.
	  * All the settings are stored into suitable variables
	  */
	void loadSettings();

	/**
	  * Load the plugins selected by the user
	  */
	void loadSelectedPlugins();

	/**
	  * Code to load a plugin, given its path and insertion positon
	  */
	bool loadPlugin(const QString& pluginPath, int insertIndex);

	/**
	  * Fix the size of the paperwidgets, and it's constituents
	  */
	void prepareWidgetView();

	/**
	  * Setup the widgets like SNI, Power, Network, etc.
	  */
	void setupWidgets();



	QMap<QString, QWidget *> m_pluginsWidgets;

protected:
	void paintEvent(QPaintEvent *pEvent);

Q_SIGNALS:
	void hideWidgets();
};
