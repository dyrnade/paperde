/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QtCore>

#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/WayQtUtils.hpp>

#include <DFL/DF6/IpcClient.hpp>

#include "papermenu.h"

namespace Paper {
namespace Menu {
class Manager;
}
}

class pageapps;
class QVariantAnimation;

/**
  * @class Paper::Menu::Manager
  * This class will handle the creation, showing, hiding and destruction
  * of papermenu on one or all screens.
  * Additionally, it will also handle the CLI requests, if any
  */
class Paper::Menu::Manager : public QObject {
	Q_OBJECT;

public:
	Manager();
	~Manager();

	/**
	  * Load the user settings.
	  */
	void loadSettings();

	/**
	  * Handle the requests from other instance. To reply to queries,
	  * write suitable data to the file descriptor @fd.
	  */
	Q_SLOT void handleMessages(QString msg, int fd);

	/**
	  * Show a layer surface on the output named @opName
	  * NOTE: This will automatically show the menu as top layer.
	  */
	void showLayerSurface(QString opName);

	/**
	  * Hide a layer surface on the output named @opName
	  */
	void hideLayerSurface();

private:
	papermenu *mInstance     = nullptr;
	WQt::LayerSurface *mSurf = nullptr;

	/**
	  * Dock size - we obtain this from the settings
	  * 40 is the default. Load the correct value in @loadSettings().
	  */
	int mDockSize = 40;

	/**
	  * Check if we are animating. If yes, then further requests will be ignored.
	  */
	bool mAnimating = false;

	/**
	  * Store the client fd when menu is showing
	  */
	int mTempFd = -1;

	/**
	  * Return an animator which will animate a layer surface when started.
	  * @surf - Layer surface to be moved
	  * @pw   - Instance of paperwidget (used to calculate the margin positions)
	  * @show - If true, the widget is being shown, otherwise hidden
	  */
	QVariantAnimation *createAnimator(WQt::LayerSurface *surf, papermenu *ui, bool show);

	/**
	  * Start an instance of WQt::LayerSurface for papermenu on screen @scrn
	  */
	bool createInstance(QScreen *scrn);

	/**
	  * Delete the above created instance of WQt::LayerSurface
	  */
	void destroyInstance();
};
