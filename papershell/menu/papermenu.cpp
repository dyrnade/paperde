﻿/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include <QtConcurrent/QtConcurrent>
#include <QGraphicsDropShadowEffect>
#include <QListWidgetItem>
#include <QKeyEvent>
#include <QScroller>
#include <QPainter>

#include <cprime/systemxdg.h>
#include <cprime/applicationdialog.h>
#include <cprime/appopenfunc.h>
#include <cprime/themefunc.h>

#include "global.h"
#include "papermenu.h"
#include "ui_papermenu.h"
#include "paper-config.h"

QMutex appLock;

papermenu::papermenu() : QWidget()
	, ui(new Ui::papermenu)
{
	setAttribute(Qt::WA_TranslucentBackground);
	setWindowFlags(Qt::BypassWindowManagerHint | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);

	ui->setupUi(this);
	setWindowTitle("Paper Menu");
	setWindowIcon(QIcon::fromTheme("papershell"));

	loadSettings();
	startSetup();
	setupAppsView();
}


papermenu::~papermenu()
{
	delete ui;
}


/**
  * @brief Loads application settings
  */
void papermenu::loadSettings()
{
	uiMode = shellSett->value("Personalization/UIMode");
}


void papermenu::startSetup()
{
	if (uiMode == 2)
	{
		iconViewIconSize = QSize(48, 48);
		ui->logout->setIconSize(QSize(24, 24));
		ui->settings->setIconSize(QSize(24, 24));
	}
	else
	{
		iconViewIconSize = QSize(64, 64);
		ui->logout->setIconSize(QSize(32, 32));
		ui->settings->setIconSize(QSize(32, 32));
	}

	ui->appsView->setIconSize(iconViewIconSize);
	ui->appsView->setGridSize(QSize(iconViewIconSize.width() * 3, iconViewIconSize.height() * 2));
	ui->logout->setIcon(CPrime::ThemeFunc::themeIcon("system-log-out-symbolic", "system-log-out", "system-log-out"));
	ui->settings->setIcon(CPrime::ThemeFunc::themeIcon("systemsettings", "settings-configure", "settings"));

	ui->user->setText(qgetenv("USER"));

	QFont btnFont(font());
	btnFont.setWeight(QFont::Bold);
	btnFont.setPointSize(14);
	ui->user->setFont(btnFont);

	connect(ui->logout, &QToolButton::clicked, [ = ](){
		hide();
		qDebug() << UtilsPath "paperlogout";
		QProcess::startDetached(UtilsPath "paperlogout", {});
	});

	connect(ui->settings, &QToolButton::clicked, [ = ](){
		hide();
		QProcess::startDetached("papersettings", {});
	});

	QFileSystemWatcher *watcher = new QFileSystemWatcher();

	watcher->addPath("/usr/share/applications/");

	connect(watcher, &QFileSystemWatcher::directoryChanged, this, &papermenu::setupAppsView);

	QScroller::grabGesture(ui->appsView, QScroller::TouchGesture);
}


void papermenu::setupAppsView()
{
	ui->appsView->clear();
	ui->appsView->setSizeAdjustPolicy(QListWidget::AdjustToContents);

	if (uiMode != 2)
	{
		setStyleSheet("QListView{background-color: rgb( 0, 0, 0, 0 ); padding: 50px 10px 50px 50px;}"
					  "QListView::item {background-color: transparent;color: palette(Text);}"
					  "QListView::item:selected {background-color: palette(Highlight); color: palette(HighlightedText);}"
					  "QListView::item:hover {background-color: palette(Highlight); color: palette(HighlightedText);}");
	}
	else
	{
		setStyleSheet("QListView{background-color: rgb( 0, 0, 0, 0 ); padding: 10px 10px 10px 10px;}"
					  "QListView::item {background-color: transparent;color: palette(Text);}"
					  "QListView::item:selected {background-color: palette(Highlight); color: palette(HighlightedText);}"
					  "QListView::item:hover {background-color: palette(Highlight); color: palette(HighlightedText);}");
	}

	QtConcurrent::run([this]{ return loadApps(); });
}


void papermenu::on_appsView_itemClicked(QListWidgetItem *item)
{
	CPrime::DesktopFile df = qvariant_cast<CPrime::DesktopFile>(item->data(Qt::UserRole + 1));

	if (df.isValid())
	{
		df.startApplicationWithArgs(QStringList());
	}

	emit hideMenu();
}


void papermenu::loadApps()
{
	appLock.lock();
	CPrime::SystemXdgMime::instance()->parseDesktops();
	CPrime::AppsList list = CPrime::SystemXdgMime::instance()->allDesktops();

	std::sort(list.begin(), list.end(), [ ](const CPrime::DesktopFile& lhs, const CPrime::DesktopFile& rhs) {
		return lhs.name() < rhs.name();
	});

	for (int i = 0; i < list.count(); i++)
	{
		CPrime::DesktopFile df = list[i];

		if (not df.isValid())
		{
			continue;
		}

		if (not df.visible())
		{
			continue;
		}

		QIcon icon = CPrime::ThemeFunc::getAppIcon(df.desktopName());
		icon = CPrime::ThemeFunc::resizeIcon(icon, ui->appsView->iconSize());

		QListWidgetItem *item = new QListWidgetItem(icon, df.name());
		item->setData(Qt::UserRole + 1, QVariant::fromValue<CPrime::DesktopFile>(df));
		item->setSizeHint(QSize(110, 100));
		ui->appsView->addItem(item);
	}

	appLock.unlock();
}


void papermenu::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);

	painter.setPen(Qt::NoPen);

	QPalette pltt(palette());
	QColor   clr1(pltt.color(QPalette::Window));

	clr1.setAlphaF(0.90);

	painter.setBrush(clr1);
	painter.drawRect(rect());
	painter.end();

	QWidget::paintEvent(pEvent);
}
