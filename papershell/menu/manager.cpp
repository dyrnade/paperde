/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include "papermenu.h"
#include "manager.h"
#include "global.h"

#include <QSequentialAnimationGroup>
#include <QVariantAnimation>
#include <QEasingCurve>
#include <QScreen>

#include <DFL/DF6/Application.hpp>

Paper::Menu::Manager::Manager()
{
	mInstance = new papermenu();

	/*
	  * TODO: Please check this fd message send request
	  */
	connect(mInstance, &papermenu::hideMenu, [this] () {
		hideLayerSurface();
		if (mTempFd)
		{
			qApp->messageClient("hidden", mTempFd);
		}
	});
}


Paper::Menu::Manager::~Manager()
{
	delete mInstance;
	delete mSurf;
}


void Paper::Menu::Manager::handleMessages(QString msg, int fd)
{
	/*
	  * TODO: Please check this fd message send request
	  */
	mTempFd = fd;
	if (msg.startsWith("toggle"))
	{
		QStringList parts = msg.split("\n");
		if (mInstance->isVisible())
		{
			hideLayerSurface();
			qApp->messageClient("hidden", fd);
		}

		else
		{
			showLayerSurface(parts[1]);
			qApp->messageClient("shown", fd);
		}
	}

	else
	{
		qWarning() << "Unhandled request:" << msg;
		qApp->messageClient("Unhandled request", fd);
	}
}


void Paper::Menu::Manager::showLayerSurface(QString opName)
{
	QScreen *screen = nullptr;

	for ( QScreen *scrn: qApp->screens())
	{
		if (scrn->name() == opName)
		{
			screen = scrn;
			break;
		}
	}

	if (not createInstance(screen))
	{
		return;
	}

	QVariantAnimation *showAnim = createAnimator(mSurf, mInstance, true);

	mAnimating = true;
	showAnim->start();

	connect(showAnim, &QVariantAnimation::finished, [ = ]() mutable {
		mAnimating = false;
	});
}


void Paper::Menu::Manager::hideLayerSurface()
{
	QVariantAnimation *hideAnim = createAnimator(mSurf, mInstance, false);

	mAnimating = true;
	hideAnim->start();

	connect(hideAnim, &QVariantAnimation::finished, [ = ]() mutable {
		mAnimating = false;
		mInstance->hide();

		destroyInstance();
	});
}


QVariantAnimation * Paper::Menu::Manager::createAnimator(WQt::LayerSurface *surf, papermenu *ui, bool show)
{
	QVariantAnimation *anim = new QVariantAnimation();

	anim->setDuration(500);
	if (show)
	{
		anim->setStartValue(-ui->width());
		anim->setEndValue(40);
		anim->setEasingCurve(QEasingCurve(QEasingCurve::OutCubic));
	}

	else
	{
		anim->setStartValue(40);
		anim->setEndValue(-ui->width());
		anim->setEasingCurve(QEasingCurve(QEasingCurve::InOutQuart));
	}

	connect(anim, &QVariantAnimation::valueChanged, [ = ]( QVariant val ) {
		surf->setMargins(QMargins(val.toInt(), 0, 0, 0));
		surf->apply();

		qApp->processEvents();
	});

	return anim;
}


bool Paper::Menu::Manager::createInstance(QScreen *scrn)
{
	if (not scrn)
	{
		return false;
	}

	if (mSurf)
	{
		destroyInstance();
	}


	QSize menuSize = scrn->size() - QSize(mDockSize, 0);

	mInstance->setFixedSize(menuSize);
	mInstance->show();

//    connect(mInstance, &papermenu::Dock::UI::toggleWidgets, this, &Paper::Dock::Manager::hideLayerSurface);

	if (WQt::Utils::isWayland())
	{
		/** wl_output corresponding to @screen */
		wl_output *output = WQt::Utils::wlOutputFromQScreen(scrn);

		mSurf = wlRegistry->layerShell()->getLayerSurface(
			mInstance->windowHandle(),      // Window Handle
			output,                         // wl_output object
			WQt::LayerShell::Top,           // Background layer
			"menu"                          // Dummy namespace
			);

		/** Anchor everywhere */
		mSurf->setAnchors(
			WQt::LayerSurface::Top |
			WQt::LayerSurface::Bottom |
			WQt::LayerSurface::Left |
			WQt::LayerSurface::Right
			);

		/** Size - when */
		mSurf->setSurfaceSize(menuSize);

		/** Nothing should move this */
		mSurf->setExclusiveZone(-1);

		/** No keyboard interaction */
		mSurf->setKeyboardInteractivity(WQt::LayerSurface::Exclusive);

		/** Default margins: hide the surface */
		mSurf->setMargins(QMargins(-mDockSize - menuSize.width(), 0, 0, 0));

		/** Apply the changes */
		mSurf->apply();

		return true;
	}

	return false;
}


void Paper::Menu::Manager::destroyInstance()
{
	if (not mSurf)
	{
		return;
	}

	delete mSurf;
	mSurf = nullptr;
}
