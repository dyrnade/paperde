/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of PaperDesktop.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include <QDir>
#include <QDebug>
#include <QProcess>
#include <QMessageBox>

#include "trackedprocess.hpp"

TrackedProcess::TrackedProcess(QString path, QStringList args) : QObject()
{
	mExecPath = path;
	mArgs << args;

	proc = new QProcess();

	connect(proc, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &TrackedProcess::tryRestart);

	mCrashCount = 0;
}


TrackedProcess::~TrackedProcess()
{
	terminate();
	delete proc;
}


void TrackedProcess::start()
{
	mStartTime = QDateTime::currentDateTime();
	proc->start(mExecPath, mArgs);
}


void TrackedProcess::restart()
{
	/** This will kill the process, and cause it to restart */
	proc->terminate();
}


int TrackedProcess::restartCount()
{
	/** Always one more than the crash count */
	return mCrashCount + 1;
}


bool TrackedProcess::isRunning()
{
	return(proc->state() == QProcess::Running);
}


void TrackedProcess::terminate()
{
	if (proc->state() != QProcess::Running)
	{
		return;
	}

	/* Disconnect everything related to this */
	proc->disconnect();
	proc->terminate();
}


void TrackedProcess::tryRestart()
{
	/** Increment the crash count */
	mCrashCount++;

	qDebug() << "Crash Data...";
	qDebug() << "================================";
	qDebug() << proc->readAll();
	qDebug() << "================================\n";

	/** If the more than 60s have elapsed, reset the crash count */
	if (mStartTime.secsTo(QDateTime::currentDateTime()) > 60)
	{
		mCrashCount = 1;        // To indicate that there has been a crash
		start();
	}

	/** It's less than 60s, and the app has crashed more than 5s. */
	else if (mCrashCount >= 5)
	{
		emit crashed();
		mCrashCount = 1;
	}

	/** It's less than 60s, but also less than 5 crashes */
	else
	{
		// We will just start it again
		proc->start(mExecPath, mArgs);
	}
}
