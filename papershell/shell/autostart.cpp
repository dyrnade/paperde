/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of PaperDesktop.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include <QDir>
#include <QDebug>
#include <QProcess>
#include <QMessageBox>

#include "autostart.hpp"

PaperAutoStart::PaperAutoStart() : QThread()
{
	/* Initialization of Session Settings */
	session = new QSettings("Paper", "Session");
}


void PaperAutoStart::start()
{
	mSessionName = qgetenv("__PAPER_SESSION_NAME");
	QThread::start();
}


void PaperAutoStart::run()
{
	/* Start default Paper Apps: SNI, Power Manager, DropDown etc */
	startPaperApps();

	/* Start apps from /etc/xdg/autostart */
	QDir xdgAutoDir("/etc/xdg/autostart");

	Q_FOREACH ( QString desktop, xdgAutoDir.entryList(QStringList() << "*.desktop"))
	{
		/* If the same file exists in user's autostart, skip it */
		if (QFile::exists(QDir::home().filePath(".config/autostart/" + desktop)))
		{
			continue;
		}

		startDesktop(xdgAutoDir.filePath(desktop));
	}

	/* Start apps from ~/.config/autostart */
	QDir homeAutoDir(QDir::home().filePath(".config/autostart/"));

	Q_FOREACH ( QString desktop, homeAutoDir.entryList(QStringList() << "*.desktop"))
	{
		startDesktop(homeAutoDir.filePath(desktop));
	}

	/* Start apps from ~/.config/autostart-scripts */
	QDir scriptAutoDir(QDir::home().filePath(".config/autostart-scripts/"));

	Q_FOREACH ( QString script, scriptAutoDir.entryList(QDir::Files))
	{
		startScript(scriptAutoDir.filePath(script));
	}

	if (mSessionName.isEmpty())
	{
		mSessionName = session->value("session").toString();

		if (mSessionName.compare("New"))
		{
			restoreSession(mSessionName);
		}
	}
}


void PaperAutoStart::startPaperApps()
{
	/**
	  * Reserved for future
	  * Nothing as of now.
	  */
}


void PaperAutoStart::startDesktop(QString desktop)
{
	QSettings desktopFile(desktop, QSettings::IniFormat);

	/*
	  * If the desktop file is not to be opened in CuboCore, skip it
	  * Example: cinnamon-settings-daemon-*.desktop should not be opened
	  */
	if (desktopFile.contains("Desktop Entry/OnlyShowIn"))
	{
		QString showOnlyIn = desktopFile.value("Desktop Entry/OnlyShowIn").toString().trimmed();

		/* If @showOnlyIn is not empty and it does not match @mSessionName, don't start the app */
		if ((not showOnlyIn.isEmpty()) and (showOnlyIn != mSessionName))
		{
			return;
		}
	}

	/* If an application is set to be Hidden; do not open it */
	if (desktopFile.value("Desktop Entry/Hidden").toBool() == true)
	{
		return;
	}

	qDebug() << "Starting desktop" << desktop;

	/* If Exec is valid; execute it */
	QString exec = desktopFile.value("Desktop Entry/Exec").toString();

	if (not exec.isEmpty())
	{
#if QT_VERSION >= 0x050E01
		QStringList exec2 = exec.trimmed().split(" ", Qt::SkipEmptyParts);
#else
		QStringList exec2 = exec.trimmed().split(" ", QString::SkipEmptyParts);
#endif

		if (QFile::exists(exec2.at(0)))
		{
			QProcess::startDetached(exec2.takeFirst(), exec2);
			return;
		}

		else
		{
			for ( QString path : qgetenv("PATH").split(':'))
			{
				if (QFile::exists(path + "/" + exec2.at(0)))
				{
					QProcess::startDetached(exec2.takeFirst(), exec2);
					return;
				}
			}
		}

		return;
	}
}


void PaperAutoStart::startScript(QString script)
{
	/* @script can be a link, get the real path */
	QString realScript = QFileInfo(script).canonicalFilePath();

	qDebug() << "Starting script" << script;
	QProcess::startDetached(realScript, {});
}


void PaperAutoStart::restoreSession(QString name)
{
	for ( QString desktop: session->value(name + "/OpenAppDesktops").toStringList())
	{
		startDesktop(desktop);
	}
}
