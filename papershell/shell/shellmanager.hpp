/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of PaperDesktop.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QtWidgets>
#include <QtGui>
#include <QtCore>

class TrackedProcess;

namespace Paper {
namespace Shell {
class Manager;
}
}

class Paper::Shell::Manager : public QObject {
	Q_OBJECT;

public:
	Manager();
	~Manager();

	void startManagement();

	/** List the tracked processes */
	void listProcesses(int);

	/** Start a tracked process if not already running */
	void startProcess(QString, QStringList);

	/** Kill a running tracked process */
	void killProcess(QString);

	/** Restart a running tracked process */
	void restartProcess(QString);

public Q_SLOTS:
	void shutdown();

	void handleMessages(QString, int);

private:
	QHash<QString, TrackedProcess *> activatedUtils;
};
