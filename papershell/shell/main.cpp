/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of PaperDesktop.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>

#include <QtCore>

#include <DFL/DF6/Application.hpp>

#include <wayqt/Registry.hpp>
#include <wayqt/WayQtUtils.hpp>

#include "global.h"
#include "shellmanager.hpp"

Paper::Settings *shellSett  = nullptr;
WQt::Registry   *wlRegistry = nullptr;

int main(int argc, char *argv[])
{
	DFL::Application app(argc, argv);

	app.setOrganizationName("CuboCore");
	app.setApplicationName("papershell");
	app.setApplicationVersion("v" PROJECT_VERSION);

	QCommandLineParser parser;

	parser.addHelpOption();         // Help
	parser.addVersionOption();      // Version

	parser.addOption({ { "l", "list-processes" }, "Run an executable as a tracked process" });
	parser.addOption({ { "s", "start-process" }, "Run an executable/paper utility as a tracked process", "name" });
	parser.addOption({ { "r", "restart-process" }, "Restart an alerady running tracked process", "process" });
	parser.addOption({ { "k", "kill-process" }, "Kill a running tracked process", "process" });

	parser.process(app);

	if (app.lockApplication())
	{
		shellSett  = new Paper::Settings("papershell");
		wlRegistry = new WQt::Registry(WQt::Wayland::display());

		wlRegistry->setup();

		Paper::Shell::Manager manager;

		QObject::connect(&app, &DFL::Application::messageFromClient, &manager, &Paper::Shell::Manager::handleMessages);

		qDebug() << "Beginning Shell Management";

		manager.startManagement();

		return app.exec();
	}

	else
	{
		if (parser.isSet("list-processes"))
		{
			QObject::connect(
				&app, &DFL::Application::messageFromServer, [ = ]( QString msg ) {
				std::cout << msg.toUtf8().constData() << std::endl;

				/**
				  * We cannot use return 0 => that will take us to app.exec();
				  * std::exit( 0 ) will exit the app with exit code = 0.
				  */
				std::exit(0);
			}
				);

			app.messageServer("list-processes");

			/** Wait till we recieve a reply, and display it. */
			app.exec();
		}

		else if (parser.isSet("start-process"))
		{
			QString proc = parser.value("start-process");
			app.messageServer("start-process\n\n" + proc + "\n\n" + parser.positionalArguments().join("\n"));
		}

		else if (parser.isSet("restart-process"))
		{
			QString proc = parser.value("restart-process");
			app.messageServer("restart-process\n" + proc);
		}

		else if (parser.isSet("kill-process"))
		{
			QString proc = parser.value("kill-process");
			app.messageServer("kill-process\n" + proc);
		}

		else
		{
			parser.showHelp();
		}

		return 0;
	}

	return 0;
}
