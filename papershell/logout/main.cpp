/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

// Local Headers
#include "global.h"
#include "logout.h"

#include <QScreen>
#include <signal.h>

#include <paperde/paperlog.h>

#include <cprime/systemxdg.h>

// Wayland Headers
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>

#include <DFL/DF6/Application.hpp>

Paper::Settings *shellSett;
WQt::Registry   *wlRegistry;

PowerDialog *setupBackground(QScreen *screen)
{
	PowerDialog *pwrDlg = new PowerDialog(screen);

	pwrDlg->show();

	if (WQt::Utils::isWayland())
	{
		/** wl_output corresponding to @screen */
		wl_output *output = WQt::Utils::wlOutputFromQScreen(screen);

		WQt::LayerSurface *cls = wlRegistry->layerShell()->getLayerSurface(
			pwrDlg->windowHandle(),    // Window Handle
			output,                    // wl_output object - for multi-monitor support
			WQt::LayerShell::Overlay,  // Background layer
			"de-widget"                // Dummy namespace
			);

		/** Anchor everywhere */
		cls->setAnchors(
			WQt::LayerSurface::Top |
			WQt::LayerSurface::Bottom |
			WQt::LayerSurface::Left |
			WQt::LayerSurface::Right
			);

		/** Size - when */
		cls->setSurfaceSize(pwrDlg->size());

		/** Nothing should move this */
		cls->setExclusiveZone(-1);

		/** No keyboard interaction */
		cls->setKeyboardInteractivity(WQt::LayerSurface::Exclusive);

		/** Apply the changes */
		cls->apply();
	}

	return pwrDlg;
}


int main(int argc, char **argv)
{
	qputenv("QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral("1"));

	QDir cache(CPrime::SystemXdg::userDir(CPrime::SystemXdg::XDG_CACHE_HOME));

	Paper::log = fopen(cache.filePath("paperde/PowerDialog.log").toLocal8Bit().data(), "a");

	qInstallMessageHandler(Paper::Logger);

	qDebug() << "------------------------------------------------------------------------";
	qDebug() << "Paper BG started at" << QDateTime::currentDateTime().toString("yyyyMMddThhmmss").toUtf8().constData();
	qDebug() << "------------------------------------------------------------------------";

	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	DFL::Application *app = new DFL::Application(argc, argv);

	app->setApplicationName("LogoutDialog");
	app->setOrganizationName("CuboCore");
	app->setApplicationVersion("v" PROJECT_VERSION);

	app->interceptSignal(SIGINT, true);
	app->interceptSignal(SIGTERM, true);
	app->interceptSignal(SIGQUIT, true);
	app->interceptSignal(SIGSEGV, true);
	app->interceptSignal(SIGABRT, true);

	if (app->lockApplication())
	{
		/* Init DFL::Settings object */
		shellSett = new Paper::Settings("papershell");

		wlRegistry = new WQt::Registry(WQt::Wayland::display());

		wlRegistry->setup();

		qDebug() << "Starting PowerDialog";

		for ( QScreen *screen: qApp->screens())
		{
			PowerDialog *dlg = setupBackground(screen);

			QObject::connect(app, &DFL::Application::screenRemoved, [ = ] ( QScreen *scrn ) {
				if (scrn == screen)
				{
					dlg->close();
					delete dlg;
				}
			});
		}

		QObject::connect(app, &QApplication::screenAdded, [app] ( QScreen *screen ) {
			PowerDialog *pwrDlg = setupBackground(screen);
			QObject::connect(app, &DFL::Application::screenRemoved, [ = ] ( QScreen *scrn ) {
				if (scrn == screen)
				{
					pwrDlg->close();
					delete pwrDlg;
				}
			});
		});

		return app->exec();
	}

	return 0;
}
