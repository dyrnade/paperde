/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include <QLabel>
#include <QScreen>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QApplication>
#include <QDBusInterface>
#include <QDBusReply>
#include <QPainter>

#include <DFL/DF6/IpcClient.hpp>
#include <DFL/DF6/Application.hpp>

#include "global.h"

#include "logout.h"

QDBusInterface session(
	"org.CuboCore.Paper.Session",
	"/org/CuboCore/Paper/Session",
	"org.CuboCore.Paper.Session",
	QDBusConnection::sessionBus()
	);

static inline bool canDo(QString action)
{
	QDBusReply<bool> reply = session.call("Can" + action);

	return reply.value();
}


PowerDialog::PowerDialog(QScreen *scrn) : QWidget()
{
	mScreen = scrn;

	setWindowFlags(Qt::BypassWindowManagerHint | Qt::FramelessWindowHint);
	setAttribute(Qt::WA_TranslucentBackground);

	loadSettings();
	prepareIpcClient();

	QPalette pltt = qApp->palette();
	pltt.setColor(QPalette::Window, Qt::black);
	pltt.setColor(QPalette::WindowText, Qt::white);

	setPalette(pltt);

	createUI();
}


PowerDialog::~PowerDialog()
{
	delete mSessionClient;

	delete lockBtn;
	delete logOutBtn;
	delete suspendBtn;
	delete hibernateBtn;
	delete powerOffBtn;
	delete rebootBtn;
	delete cancelBtn;

	delete mScreen;
}


void PowerDialog::prepareIpcClient()
{
	QString orgName = "CuboCore";
	QString appName = "papersessionmanager";

	QString sockDir("%1/%2/%3");

	sockDir = sockDir.arg(QStandardPaths::writableLocation(QStandardPaths::RuntimeLocation))
				 .arg(orgName.replace(" ", ""))
				 .arg(QString(qgetenv("XDG_SESSION_ID")));

	mSessionSockPath = QString("%1/%2.socket").arg(sockDir).arg(appName.replace(" ", ""));

	mSessionClient = new DFL::IPC::Client(mSessionSockPath, this);
	mSessionClient->connectToServer();
}


void PowerDialog::createUI()
{
	QSize iconSize = QSize(110, 80);

	if (uiMode == 2)
	{
		iconSize = QSize(64, 64);
	}

	lockBtn = new QToolButton;
	lockBtn->setText("Lock");
	lockBtn->setToolTip("Lock the system");
	lockBtn->setIcon(QIcon::fromTheme("system-lock-screen"));
	connect(lockBtn, &QToolButton::pressed, this, &PowerDialog::lock);

	logOutBtn = new QToolButton;
	logOutBtn->setText("Logout");
	logOutBtn->setToolTip("Logout as the current user");
	logOutBtn->setIcon(QIcon::fromTheme("system-log-out"));
	logOutBtn->setEnabled(false);
	connect(logOutBtn, &QToolButton::pressed, this, &PowerDialog::logout);

	suspendBtn = new QToolButton;
	suspendBtn->setText("Suspend");
	suspendBtn->setToolTip("Suspend the system to RAM");
	suspendBtn->setIcon(QIcon::fromTheme("system-suspend"));
	suspendBtn->setEnabled(false);
	connect(suspendBtn, &QToolButton::pressed, this, &PowerDialog::suspend);

	hibernateBtn = new QToolButton;
	hibernateBtn->setText("Hibernate");
	hibernateBtn->setToolTip("Suspend the system to Disk");
	hibernateBtn->setIcon(QIcon::fromTheme("system-suspend-hibernate"));
	hibernateBtn->setEnabled(false);
	connect(hibernateBtn, &QToolButton::pressed, this, &PowerDialog::hibernate);

	powerOffBtn = new QToolButton;
	powerOffBtn->setText("Power Off");
	powerOffBtn->setToolTip("Power off the system");
	powerOffBtn->setIcon(QIcon::fromTheme("system-shutdown"));
	powerOffBtn->setEnabled(false);
	connect(powerOffBtn, &QToolButton::pressed, this, &PowerDialog::poweroff);

	rebootBtn = new QToolButton;
	rebootBtn->setText("Reboot");
	rebootBtn->setToolTip("Reboot the system");
	rebootBtn->setIcon(QIcon::fromTheme("system-reboot"));
	rebootBtn->setEnabled(false);
	connect(rebootBtn, &QToolButton::pressed, this, &PowerDialog::reboot);

	cancelBtn = new QToolButton;
	cancelBtn->setText("Cancel");
	cancelBtn->setToolTip("Do nothing");
	cancelBtn->setIcon(QIcon::fromTheme("dialog-close"));
	connect(cancelBtn, &QToolButton::pressed, this, &QWidget::close);

	int width  = mScreen->size().width();
	int height = mScreen->size().height();

	setFixedSize(mScreen->size());

	// Layout
	QBoxLayout *lyt    = nullptr;
	QBoxLayout *layout = new QHBoxLayout();

	if (width > height)
	{
		qDebug() << "orientation: Landscape";
		lyt = new QHBoxLayout();
	}
	else
	{
		qDebug() << "orientation: Portrait";
		lyt = new QVBoxLayout();
	}

	for (QToolButton *btn : { lockBtn, logOutBtn, suspendBtn, hibernateBtn, powerOffBtn, rebootBtn, cancelBtn })
	{
		btn->setIconSize(iconSize);

		if (width > height)
		{
			btn->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
		}
		else
		{
			btn->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
			btn->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
		}

		btn->setAutoRaise(true);
	}

	lyt->addStretch();
	lyt->setContentsMargins(5, 5, 5, 5);
	lyt->addWidget(lockBtn);
	lyt->addWidget(logOutBtn);
	lyt->addWidget(suspendBtn);
	lyt->addWidget(hibernateBtn);
	lyt->addWidget(powerOffBtn);
	lyt->addWidget(rebootBtn);
	lyt->addWidget(cancelBtn);
	lyt->addStretch();

	layout->addStretch();
	layout->addLayout(lyt);
	layout->addStretch();

	setLayout(layout);

	/** Wait 100 ms for connection */
	if (not mSessionClient->waitForRegistered(10 * 1000))
	{
		qCritical() << "Unable to connect to" << mSessionSockPath;
		return;
	}

	/** Connected to paper session manager. So we can logout. */
	logOutBtn->setEnabled(true);

	mSessionClient->sendMessage("CanSuspend");
	mSessionClient->waitForReply(10 * 1000);            // 10 ms wait
	suspendBtn->setEnabled(mSessionClient->reply() == "true" ? true : false);
	qDebug() << "CanSuspend" << mSessionClient->reply();

	mSessionClient->sendMessage("CanHibernate");
	mSessionClient->waitForReply(10 * 1000);            // 10 ms wait
	hibernateBtn->setEnabled(mSessionClient->reply() == "true" ? true : false);
	qDebug() << "CanHibernate" << mSessionClient->reply();

	mSessionClient->sendMessage("CanPowerOff");
	mSessionClient->waitForReply(10 * 1000);            // 10 ms wait
	powerOffBtn->setEnabled(mSessionClient->reply() == "true" ? true : false);
	qDebug() << "CanPowerOff" << mSessionClient->reply();

	mSessionClient->sendMessage("CanReboot");
	mSessionClient->waitForReply(10 * 1000);            // 10 ms wait
	rebootBtn->setEnabled(mSessionClient->reply() == "true" ? true : false);
	qDebug() << "CanReboot" << mSessionClient->reply();
}


void PowerDialog::lock()
{
	// TODO
	// need to use sway lock to lock the system
	QProcess::startDetached("swaylock", {});

	qApp->quit();
}


void PowerDialog::logout()
{
	/** Wait 100 ms for connection */
	if (not mSessionClient->waitForRegistered(100 * 1000))
	{
		return;
	}

	mSessionClient->sendMessage("logout");

	/* We manually close this app */
	qApp->quit();
}


void PowerDialog::suspend()
{
	/** Wait 100 ms for connection */
	if (not mSessionClient->waitForRegistered(100 * 1000))
	{
		return;
	}

	mSessionClient->sendMessage("suspend");

	/* We manually close this app */
	qApp->quit();
}


void PowerDialog::hibernate()
{
	/** Wait 100 ms for connection */
	if (not mSessionClient->waitForRegistered(100 * 1000))
	{
		return;
	}

	mSessionClient->sendMessage("hibernate");

	/* We manually close this app */
	qApp->quit();
}


void PowerDialog::poweroff()
{
	/** Wait 100 ms for connection */
	if (not mSessionClient->waitForRegistered(100 * 1000))
	{
		return;
	}

	mSessionClient->sendMessage("shutdown");

	/* We manually close this app */
	qApp->quit();
}


void PowerDialog::reboot()
{
	/** Wait 100 ms for connection */
	if (not mSessionClient->waitForRegistered(100 * 1000))
	{
		return;
	}

	mSessionClient->sendMessage("reboot");

	/* We manually close this app */
	qApp->quit();
}


/**
  * @brief Loads application settings
  */
void PowerDialog::loadSettings()
{
	uiMode = shellSett->value("Personalization/UIMode");
}


void PowerDialog::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);

	QPalette pltt(palette());
	QColor   bg(pltt.color(QPalette::Window));

	bg.setAlphaF(0.8);

	painter.setPen(Qt::NoPen);
	painter.setBrush(bg);

	painter.drawRect(rect());
	painter.end();

	QWidget::paintEvent(pEvent);
}
