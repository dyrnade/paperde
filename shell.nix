{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/d840126a0890621e7b220894d749132dd4bde6a0.tar.gz") {} }:
with import <nixpkgs> {};
let
  gccForLibs = pkgs.stdenv.cc.cc;
  ipc = pkgs.callPackage ./ipc {};
  login1 = pkgs.callPackage ./login1 {};
  status-notifier = pkgs.callPackage ./status-notifier {};
  wayqt = pkgs.callPackage ./wayqt {};
  applications = pkgs.callPackage ./applications { inherit ipc; };
  libdbusmenu-qt = pkgs.callPackage ./libdbusmenu-qt { };
  libcsys = pkgs.callPackage ./libcsys { };
  libcprime = pkgs.callPackage ./libcprime { };
  paperde = pkgs.callPackage ./paperde { inherit libcprime libcsys wayqt status-notifier ipc applications login1; };
in clangStdenv.mkDerivation {
    name = "test-env";
    buildInputs = [
    paperde
    libcprime
    libcsys
    libdbusmenu-qt
    ipc
    login1
    status-notifier
    wayqt
    applications
    pkgs.bashInteractive
    pkgs.git
    pkgs.gcc8
    #pkgs.libsForQt5.libdbusmenu
    pkgs.lld
    pkgs.gdb
    pkgs.cmake
    pkgs.gnumake
    pkgs.qt6.full
    #pkgs.qt6.qmake
    pkgs.ninja
    #pkgs.qtcreator
    #pkgs.jetbrains.clion
    pkgs.meson
    pkgs.qt6.qttools
    pkgs.ninja
    pkgs.pkgconfig
    pkgs.wayland
    pkgs.wayland-protocols
    pkgs.xwayland
    pkgs.xdg-user-dirs
    #pkgs.CuboCore.libcsys
    #pkgs.CuboCore.libcprime
    pkgs.xdg-desktop-portal
    pkgs.xdg-desktop-portal-kde    
    pkgs.xdg-desktop-portal-gtk    
    pkgs.xdg-desktop-portal-wlr
    pkgs.wayfire
    pkgs.udisks2
];
  # where to find libgcc
  NIX_LDFLAGS="-L${gccForLibs}/lib/gcc/${targetPlatform.config}/${gccForLibs.version} -L ${ipc}/lib";
  # teach clang about C startup file locations
  CFLAGS="-B${gccForLibs}/lib/gcc/${targetPlatform.config}/${gccForLibs.version} -B ${stdenv.cc.libc}/lib";
  cmakeFlags = [
    "-DGCC_INSTALL_PREFIX=${gcc}"
    "-DC_INCLUDE_DIRS=${stdenv.cc.libc.dev}/include"
    "-GNinja"
    # Debug for debug builds
    "-DCMAKE_BUILD_TYPE=Release"
    # inst will be our installation prefix
    "-DCMAKE_INSTALL_PREFIX=../inst"
    "-DLLVM_INSTALL_TOOLCHAIN_ONLY=ON"
    # change this to enable the projects you need
    "-DLLVM_ENABLE_PROJECTS=clang"
    # enable libcxx* to come into play at runtimes
    "-DLLVM_ENABLE_RUNTIMES=libcxx;libcxxabi"
    # this makes llvm only to produce code for the current platform, this saves CPU time, change it to what you need
    "-DLLVM_TARGETS_TO_BUILD=host"
  ];
}
