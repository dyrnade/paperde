/*
  *
  * This file is a part of Paper Settings.
  * Paper Settings is the Settings app to manage Paper Desktop settings
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#include <QLineEdit>
#include <QComboBox>
#include <QSpinBox>
#include <QMessageBox>
#include <QUiLoader>

#include <paperde/papersettings.h>

#include "settings.h"
#include "ui_settings.h"

#include <cprime/desktopfile.h>
#include <cprime/themefunc.h>
#include <cprime/applicationdialog.h>


Paper::SettingsUI::SettingsUI(QWidget *parent) : QMainWindow(parent)
	, ui(new Ui::settings)
	, shellSett(new Paper::Settings("papershell"))
	, loading(true)
{
	ui->setupUi(this);

	loadSettings();
	startSetup();
	loading = false;
}


Paper::SettingsUI::~SettingsUI()
{
	delete shellSett;
	delete ui;
}


void Paper::SettingsUI::loadSettings()
{
	qDebug() << "Loading settings...";

	uiMode     = shellSett->value("Personalization/UIMode");
	autoDetect = shellSett->value("Personalization/AutoDetect");
}


void Paper::SettingsUI::startSetup()
{
	// Set the transparent color to the main widget
	QPalette p(palette());

	p.setColor(QPalette::Base, Qt::transparent);
	this->setPalette(p);


	if (uiMode == 2)
	{
		ui->content->setVisible(false);
		ui->leftNavShowHide->setIconSize(QSize(32, 32));
	}
	else
	{
		ui->content->setVisible(true);

		ui->topNav->setVisible(false);
		ui->leftNav->setFixedWidth(200);
	}


	connect(ui->navList, &QListWidget::itemClicked, [ = ](QListWidgetItem *item) {
		QString text = item->text().toLower();

		if (text.contains("personal"))
		{
			loadPage("personal");
		}

		else if (text.contains("about"))
		{
			loadPage("about");
		}

		else if (text.contains("display"))
		{
			loadPage("display");
		}

		else if (text.contains("home"))
		{
			loadPage("home");
		}

		else if (text.contains("network"))
		{
			loadPage("network");
		}

		else if (text.contains("power"))
		{
			loadPage("power");
		}

		else if (text.contains("system"))
		{
			loadPage("system");
		}

		else if (text.contains("users"))
		{
			loadPage("users");
		}

		else if (text.contains("advanced"))
		{
			loadPage("advanced");
		}

		else if (text.contains("restore defaults"))
		{
			restoreDefaultSettings();
		}
	});

	if (uiMode != 2)
	{
		// Load the home page
		loadPage("home");
	}
}


/**
  * FIXME: Transparency lost while new page is loaded
  */
void Paper::SettingsUI::loadPage(const QString& page)
{
	QUiLoader loader;

	QFile uiFile(":/pages/" + page + ".ui");

	uiFile.open(QFile::ReadOnly);

	settingsWidget = loader.load(&uiFile, this);

	if (uiMode == 2)
	{
		ui->content->setVisible(true);
		ui->leftNav->setVisible(false);
		ui->selectedPage->setText(page.toUpper());
	}
	ui->scrollAreaPage->setWidget(settingsWidget);

	/* If the UI was loaded properly */
	if (loader.errorString().isEmpty())
	{
		if (page == "personal")
		{
			loadPersonalizations();
		}

		else if (page == "about")
		{
			loadAbout();
		}

		else if (page == "display")
		{
			loadDisplay();
		}

		else if (page == "home")
		{
			loadHome();
		}

		else if (page == "network")
		{
			loadNetwork();
		}

		else if (page == "others")
		{
			loadOthers();
		}

		else if (page == "power")
		{
			loadPower();
		}

		else if (page == "sound")
		{
			loadSound();
		}

		else if (page == "system")
		{
			loadSystem();
		}

		else if (page == "users")
		{
			loadUsers();
		}

		else if (page == "advanced")
		{
			loadAdvanced();
		}
	}
}


void Paper::SettingsUI::valueChanged()
{
	/* We're loading the settings, expect changes to widgets */
	/* These changes are not to be saved. */
	if (loading)
	{
		return;
	}

	/* Something changes and a signal was sent to this slot: Get its sender */
	QObject *xsender = sender();
	QString key      = xsender->property("Setting").toString();

	if (key.isEmpty())
	{
		return;
	}

	/* ========== Store the settings that changed ========== */

	// QAbstractButton - Manages QToolButton, QPushButton, QRadioButton and QCheckBox
	// Generally, only thing that can change is `checked` status
	// In case of favourite apps buttons, we need to store more data.
	// They will have an additional property "FavApp" defined (should be valid).
	QAbstractButton *btn = qobject_cast<QAbstractButton *>(xsender);

	if (btn)
	{
		if (btn->property("FavApp").isValid())
		{
			QStringList favApps = shellSett->value("DockBar/Favourite");

			QString btnTxt = btn->text().replace("&", "");

			if (btnTxt.startsWith("@remove-"))
			{
				favApps.removeOne(btnTxt.replace("@remove-", ""));
				btn->setText("+");
			}

			else if ((btnTxt != "...") and (btnTxt != "+"))
			{
				favApps << btnTxt;
			}

			qDebug() << favApps << btnTxt;
			shellSett->setValue("DockBar/Favourite", favApps);
			return;
		}

		shellSett->setValue(key, btn->isChecked());
		return;
	}

	// LineEdit
	QLineEdit *edit = qobject_cast<QLineEdit *>(xsender);

	if (edit)
	{
		shellSett->setValue(key, edit->text());
		return;
	}

	// ComboBox
	QComboBox *menu = qobject_cast<QComboBox *>(xsender);

	// Save the current index or the currrent text? Choose wisely
	if (menu)
	{
		// If the key has "Size" in it, it's better to save it as QSize(...)
		if (key.contains("size", Qt::CaseInsensitive))
		{
			int size = menu->currentText().toInt();
			shellSett->setValue(key, QSize(size, size));
			qDebug() << "QSize " << QSize(size, size) << size;
		}

		// If we have to save the text: Ex: Icon Size drop-down
		else if (menu->property("SaveText").isValid())
		{
			shellSett->setValue(key, menu->currentText());
		}

		// Else save the current index: Ex: Touch mode drop-down
		else
		{
			shellSett->setValue(key, menu->currentIndex());
		}

		return;
	}

	// SpinBox
	QSpinBox *spin = qobject_cast<QSpinBox *>(xsender);

	// Save the current index or the currrent text? Choose wisely
	if (spin)
	{
		shellSett->setValue(key, spin->value());
		return;
	}
}


void Paper::SettingsUI::addRemoveApp(int i)
{
	QToolButton *appBtn = qobject_cast<QToolButton *>(favAppsGrp->button(i));

	/* If button has text, then add app */
	if (appBtn->text() == "+")
	{
		CPrime::ApplicationDialog *dialog = new CPrime::ApplicationDialog(QSize(32, 32), this);

		if (dialog->exec())
		{
			QString curr = dialog->getCurrentLauncher();

			if (not curr.isEmpty())
			{
				CPrime::DesktopFile file(curr);
				appBtn->setText(curr);
				appBtn->setIcon(CPrime::ThemeFunc::getAppIcon(file.desktopName()));
				appBtn->setToolButtonStyle(Qt::ToolButtonIconOnly);

				emit appBtn->clicked();
			}
		}
	}
	else     /* Button has some text */
	{
		appBtn->setIcon(QIcon());
		appBtn->setText("@remove-" + appBtn->text());
	}
}


void Paper::SettingsUI::restoreDefaultSettings()
{
	int reply = QMessageBox::question(
		this,
		"PaperDesktop Settings | Restore Defaults?",
		"By clicking <tt>Restore Defaults</tt>, you're about to reset <b>ALL</b> the settings to defaults values. "
		"You will lose all the settings and personalizations. This action cannot be undone. Do you want to continue?",
		QMessageBox::Yes | QMessageBox::No
		);

	if (reply == QMessageBox::Yes)
	{
		// TODO implement clear settings
//		shellSett->clearSettings();
	}
}


void Paper::SettingsUI::on_leftNavShowHide_clicked()
{
	if (ui->content->isVisible())
	{
		ui->navList->setCurrentRow(-1);
		ui->content->setVisible(false);
		ui->leftNav->setVisible(true);
	}
	else
	{
		ui->content->setVisible(true);
		ui->leftNav->setVisible(false);
	}
}


QString Paper::SettingsUI::qsizeToString(QSize size)
{
	return QString("%1").arg(size.width());
}
