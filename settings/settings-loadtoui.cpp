/*
  *
  * This file is a part of Paper Settings.
  * Paper Settings is the Settings app to manage Paper Desktop settings
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#include <QtWidgets>

#include <paperde/papersettings.h>

#include "settings.h"
#include "ui_settings.h"

#include <cprime/systemxdg.h>
#include <cprime/themefunc.h>
#include <cprime/cplugininterface.h>

/*
  *
  * @s96abrar, @rahmanshaber
  *
  * Loading the saved settings to the UI involves a lot of code.
  * Putting that code here will help us maintain the code more easily.
  *
  */
void Paper::SettingsUI::loadAbout()
{
	// Load About Settings
}


void Paper::SettingsUI::loadBluetooth()
{
	// Load Bluetooth Settings
}


void Paper::SettingsUI::loadDisplay()
{
	// Load Display Settings
}


void Paper::SettingsUI::loadHome()
{
	// Load Home Settings
	qDebug() << "Loading 'Home' page";

	auto *systemMime = CPrime::SystemXdgMime::instance();
	auto *listWidget = settingsWidget->findChild<QListWidget *>("appList");

	connect(listWidget, &QListWidget::itemClicked, [ = ](QListWidgetItem *item) {
		auto df = item->data(Qt::UserRole).value<CPrime::DesktopFile>();
		df.startApplication();
	});

	QStringList selectedApps;

	selectedApps << "Wayfire Config Manager" << "Kvantum Manager" << "Qt5 Settings" << "CoreGarage";

	Q_FOREACH (auto app, systemMime->allDesktops())
	{
		if (selectedApps.contains(app.name()))
		{
			auto *item = new QListWidgetItem(app.name());
			item->setData(Qt::UserRole, QVariant::fromValue<CPrime::DesktopFile>(app));
			item->setIcon(QIcon::fromTheme(app.icon()));
			item->setTextAlignment(Qt::AlignCenter);
			listWidget->addItem(item);
		}
	}
}


void Paper::SettingsUI::loadNetwork()
{
	// Load Network Settings
}


void Paper::SettingsUI::loadOthers()
{
	// Load Others Settings
}


void Paper::SettingsUI::loadPersonalizations()
{
	qDebug() << "Loading 'Personalization' page";

	QToolButton *tBtn;
	QComboBox   *combo;
	QLineEdit   *lEdit;
	QSpinBox    *iSpin;

	// Look and Feel
	tBtn = settingsWidget->findChild<QToolButton *>("autoDetect");
	tBtn->setChecked(shellSett->value("Personalization/AutoDetect"));
	connect(tBtn, &QToolButton::toggled, this, &Paper::SettingsUI::valueChanged);

	combo = settingsWidget->findChild<QComboBox *>("modeCombo");
	combo->setCurrentIndex((int)shellSett->value("Personalization/UIMode"));
	connect(combo, qOverload<int>(&QComboBox::currentIndexChanged), this, &Paper::SettingsUI::valueChanged);
	if (shellSett->value("Personalization/AutoDetect"))
	{
		combo->setDisabled(true);
	}

	/* At this stage tBtn still points to autoDetect button */
	connect(
		tBtn, &QToolButton::toggled, [ = ](bool checked) {
		if (checked)
		{
			combo->setDisabled(true);
		}

		else
		{
			combo->setEnabled(true);
		}
	}
		);

	lEdit = settingsWidget->findChild<QLineEdit *>("bgPath");
	lEdit->setText(shellSett->value("Personalization/Background"));
	connect(lEdit, &QLineEdit::textChanged, this, &Paper::SettingsUI::valueChanged);

	tBtn = settingsWidget->findChild<QToolButton *>("selectBG");
	connect(tBtn, &QToolButton::clicked, [ = ]() {
		QString bgFile = QFileDialog::getOpenFileName(this, "Select a background image",
													  QDir::homePath(), "Images (*.png *.jpg *.jpeg *.svg)");
		if (not bgFile.isEmpty())
		{
			lEdit->setText(bgFile);
		}
	});

	combo = settingsWidget->findChild<QComboBox *>("wallpaperPos");
	combo->setCurrentIndex((int)shellSett->value("Personalization/WallpaperPosition"));
	connect(combo, qOverload<int>(&QComboBox::currentIndexChanged), this, &Paper::SettingsUI::valueChanged);

	// Dockbar
//	combo = settingsWidget->findChild<QComboBox *>("cmbDockBarPos");
//	combo->setCurrentText(shellSett->value("DockBar/Anchor"));
//	connect(combo, qOverload<int>(&QComboBox::currentIndexChanged), this, &Paper::SettingsUI::valueChanged);

	tBtn = settingsWidget->findChild<QToolButton *>("autoHideDock");
	tBtn->setChecked(shellSett->value("DockBar/AutoHide"));
	connect(tBtn, &QToolButton::toggled, this, &Paper::SettingsUI::valueChanged);

	iSpin = settingsWidget->findChild<QSpinBox *>("dockHideDuration");
	iSpin->setValue(shellSett->value("DockBar/HideDuration"));
	connect(iSpin, qOverload<int>(&QSpinBox::valueChanged), this, &Paper::SettingsUI::valueChanged);

	combo = settingsWidget->findChild<QComboBox *>("cmbDockIconSize");
	combo->setCurrentText(qsizeToString(shellSett->value("DockBar/IconSize")));
	connect(combo, qOverload<int>(&QComboBox::currentIndexChanged), this, &Paper::SettingsUI::valueChanged);

	favAppsGrp = new QButtonGroup();
	QStringList appsList = shellSett->value("DockBar/Favourite");

	for (int i = 1; i <= 5; i++)
	{
		tBtn = settingsWidget->findChild<QToolButton *>(QString("favApp%1Btn").arg(i));
		connect(tBtn, &QToolButton::clicked, this, &Paper::SettingsUI::valueChanged);

		favAppsGrp->addButton(tBtn, i - 1);

		/* We have an app already set */
		if (appsList.count() > (i - 1))
		{
			tBtn->setIcon(CPrime::ThemeFunc::getAppIcon(appsList.at(i - 1)));
			tBtn->setText(appsList.at(i - 1));
		}
		else
		{
			tBtn->setIcon(QIcon());
			tBtn->setText("+");
		}
	}
	connect(favAppsGrp, &QButtonGroup::idPressed, this, &Paper::SettingsUI::addRemoveApp);

	/* Connections for add/remove plugin paths */
	tBtn = settingsWidget->findChild<QToolButton *>("addPluginPathBtn");
	connect(tBtn, &QToolButton::clicked, this, &Paper::SettingsUI::addPluginPath);

	tBtn = settingsWidget->findChild<QToolButton *>("removePluginPathBtn");
	connect(tBtn, &QToolButton::clicked, this, &Paper::SettingsUI::removePluginPath);

	tBtn = settingsWidget->findChild<QToolButton *>("movePluginUp");
	connect(tBtn, &QToolButton::clicked, this, &Paper::SettingsUI::movePluginUp);

	tBtn = settingsWidget->findChild<QToolButton *>("movePluginDown");
	connect(tBtn, &QToolButton::clicked, this, &Paper::SettingsUI::movePluginDown);

	auto *pluginList = settingsWidget->findChild<QListWidget *>("pluginsList");

	connect(pluginList, &QListWidget::itemChanged, this, &Paper::SettingsUI::pluginSelectionChanged);

	loadPluginPaths();
	loadPlugins();
}


void Paper::SettingsUI::loadPower()
{
	// Load Power Settings
	auto logout = settingsWidget->findChild<QToolButton *>("btnLogout");

	connect(logout, &QToolButton::clicked, [ = ]() {
		QProcess::startDetached("papershell", { "--logout" });
	});
}


void Paper::SettingsUI::loadSound()
{
	// Load Sound Settings
}


void Paper::SettingsUI::loadSystem()
{
	// Load System Settings
}


void Paper::SettingsUI::loadUsers()
{
	qDebug() << "Loading 'Users' page";

	// Load Users Settings
	QToolButton *btnTerminals, *btnFileManager, *btnImageEditor, *btnMetaData, *btnSearchApp, *btnRenamer;
	QLineEdit   *terminals, *fileManager, *imageEditor, *metaData, *searchApp, *renamer;

	QString oldAppName;

	terminals   = settingsWidget->findChild<QLineEdit *>("terminals");
	fileManager = settingsWidget->findChild<QLineEdit *>("fileManager");
	imageEditor = settingsWidget->findChild<QLineEdit *>("imageEditor");
	metaData    = settingsWidget->findChild<QLineEdit *>("metadataViewer");
	searchApp   = settingsWidget->findChild<QLineEdit *>("searchApp");
	renamer     = settingsWidget->findChild<QLineEdit *>("batchRenamer");

	btnTerminals   = settingsWidget->findChild<QToolButton *>("setTerminals");
	btnFileManager = settingsWidget->findChild<QToolButton *>("setFileManager");
	btnImageEditor = settingsWidget->findChild<QToolButton *>("setImageEditor");
	btnMetaData    = settingsWidget->findChild<QToolButton *>("setMetadataViewer");
	btnSearchApp   = settingsWidget->findChild<QToolButton *>("setSearchApp");
	btnRenamer     = settingsWidget->findChild<QToolButton *>("setRenamerApp");

	// Preferred Applications
	oldAppName = CPrime::SystemDefaultApps::getDefaultApp(CPrime::DefaultAppCategory::Terminal);
	terminals->setText(oldAppName);

	oldAppName = CPrime::SystemDefaultApps::getDefaultApp(CPrime::DefaultAppCategory::FileManager);
	fileManager->setText(oldAppName);

	oldAppName = CPrime::SystemDefaultApps::getDefaultApp(CPrime::DefaultAppCategory::ImageEditor);
	imageEditor->setText(oldAppName);

	oldAppName = CPrime::SystemDefaultApps::getDefaultApp(CPrime::DefaultAppCategory::MetadataViewer);
	metaData->setText(oldAppName);

	QString dSearchApp = CPrime::SystemDefaultApps::getDefaultApp(CPrime::DefaultAppCategory::SearchApp);

	searchApp->setText(oldAppName);

	QString dbatchRenamer = CPrime::SystemDefaultApps::getDefaultApp(CPrime::DefaultAppCategory::BatchRenamer);

	renamer->setText(oldAppName);

	QList<QLineEdit *>   leditArray = { terminals, fileManager, imageEditor, metaData, searchApp, renamer };
	QList<QToolButton *> btnArray   = { btnTerminals, btnFileManager, btnImageEditor, btnMetaData, btnSearchApp, btnRenamer };

	connect(terminals, &QLineEdit::textChanged, [ = ](const QString& text) {
		CPrime::SystemDefaultApps::setDefaultApp(CPrime::Terminal, text);
	});

	connect(fileManager, &QLineEdit::textChanged, [ = ](const QString& text) {
		CPrime::SystemDefaultApps::setDefaultApp(CPrime::FileManager, text);
	});

	connect(imageEditor, &QLineEdit::textChanged, [ = ](const QString& text) {
		CPrime::SystemDefaultApps::setDefaultApp(CPrime::ImageEditor, text);
	});

	connect(metaData, &QLineEdit::textChanged, [ = ](const QString& text) {
		CPrime::SystemDefaultApps::setDefaultApp(CPrime::MetadataViewer, text);
	});

	connect(searchApp, &QLineEdit::textChanged, [ = ](const QString& text) {
		CPrime::SystemDefaultApps::setDefaultApp(CPrime::SearchApp, text);
	});

	connect(renamer, &QLineEdit::textChanged, [ = ](const QString& text) {
		CPrime::SystemDefaultApps::setDefaultApp(CPrime::BatchRenamer, text);
	});

	for (int i = 0; i < btnArray.count(); i++)
	{
		auto tBtn = btnArray[i];
		connect(tBtn, &QToolButton::clicked, [ = ]() {
			QString appName = getSelectedApp();
			if (appName.count())
			{
				leditArray[i]->setText(appName);
			}
		});
	}
}


#include <cprime/applicationdialog.h>

QString Paper::SettingsUI::getSelectedApp()
{
	// Select application in the dialog
	auto *dialog = new CPrime::ApplicationDialog(QSize(32, 32), this);

	if (dialog->exec())
	{
		if (dialog->getCurrentLauncher().compare("") != 0)
		{
			return(dialog->getCurrentLauncher());
		}
	}

	return QString();
}


void Paper::SettingsUI::loadAdvanced()
{
	qDebug() << "Loading advanced page...";

	auto availableLogs = settingsWidget->findChild<QListWidget *>("availableLogs");
	auto currLogText   = settingsWidget->findChild<QPlainTextEdit *>("currLogText");
	auto btnCopyLog    = settingsWidget->findChild<QToolButton *>("btnCopyLog");

	if (!availableLogs && !currLogText)
	{
		return;
	}

	availableLogs->clear();
	currLogText->clear();

	connect(btnCopyLog, &QToolButton::clicked, [ = ]() {
		auto clipboard = qApp->clipboard();
		clipboard->setText(currLogText->toPlainText());
	});

	connect(availableLogs, &QListWidget::itemClicked, [ = ](QListWidgetItem *item) {
		QFile file(item->data(Qt::UserRole).toString());
		file.open(QIODevice::ReadOnly);
		currLogText->setPlainText(file.readAll());
		file.close();
	});

	QDirIterator it(QString("%1/.cache/paperde/").arg(QDir::homePath()), QDir::Files);

	while (it.hasNext())
	{
		it.next();
		auto item = new QListWidgetItem(it.fileName());
		item->setData(Qt::UserRole, it.filePath());
		availableLogs->addItem(item);
	}
}
