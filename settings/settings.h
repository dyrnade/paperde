/*
  *
  * This file is a part of Paper Settings.
  * Paper Settings is the Settings app to manage Paper Desktop settings
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#pragma once

#include <QMainWindow>
#include <QButtonGroup>
#include <QVersionNumber>
#include <QListWidgetItem>

class QToolButton;

namespace Paper {
class Settings;
class SettingsUI;
}

namespace Ui {
class settings;
}

class Paper::SettingsUI : public QMainWindow {
	Q_OBJECT

public:
	explicit SettingsUI(QWidget *parent = nullptr);
	~SettingsUI();

	void loadPage(const QString&);

private slots:
	// Settings change slot to save settings which is changed
	void valueChanged();

	void restoreDefaultSettings();
	void on_leftNavShowHide_clicked();

private:
	Ui::settings *ui;
	Paper::Settings *shellSett;

	/* If we are loading, then don't care to save the settings */
	bool loading;

	int uiMode;
	bool autoDetect;
	QStringList m_selectedPlugins;
	QStringList m_pluginsFolders;

	typedef struct plugin_info_t
	{
		QString        name;
		QVersionNumber version;
	} PluginInfo;

	QMap<QString, PluginInfo> plugins;

	QButtonGroup *favAppsGrp;
	QWidget *settingsWidget;

	// Helper functions
	QString qsizeToString(QSize size);

	// Initial loading of the paper-settings
	void loadSettings();
	void startSetup();

	// Load all the individual pages
	void loadAbout();
	void loadBluetooth();
	void loadDisplay();
	void loadHome();
	void loadNetwork();
	void loadOthers();
	void loadPersonalizations();
	void loadPower();
	void loadSound();
	void loadSystem();
	void loadUsers();
	void loadAdvanced();

	/* System page */
	QString getSelectedApp();

	/* Personal Page */
	void addRemoveApp(int i);
	void loadPluginPaths();
	void addPluginPath();
	void removePluginPath();
	void loadPlugins();
	void pluginSelectionChanged(QListWidgetItem *);
	void movePluginUp();
	void movePluginDown();
	void sortCheckedAtFirst();
};
