/*
  *
  * This file is a part of Paper Settings.
  * Paper Settings is the Settings app to manage Paper Desktop settings
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#include <iostream>

#include <QCommandLineParser>
#include <QApplication>
#include <paperde/paperlog.h>
#include "settings.h"

#include <cprime/capplication.h>
#include <cprime/systemxdg.h>

#include <paper-config.h>

int main(int argc, char *argv[])
{
	QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts); // Added for a warning "Qt WebEngine seems to be initialized from a plugin."
	QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	QDir cache(CPrime::SystemXdg::userDir(CPrime::SystemXdg::XDG_CACHE_HOME));

	Paper::log = fopen(cache.filePath("paperde/PaperSettings.log").toLocal8Bit().data(), "a");

	qInstallMessageHandler(Paper::Logger);

	qDebug() << "------------------------------------------------------------------------";
	qDebug() << "Paper Settings started at" << QDateTime::currentDateTime().toString("yyyyMMddThhmmss").toUtf8().constData();
	qDebug() << "------------------------------------------------------------------------";

	CPrime::CApplication app(APPNAME, argc, argv);

	if (app.isRunning())
	{
		return not app.sendMessage("");
	}

	// Set application info
	app.setOrganizationName("CuboCore");
	app.setApplicationName("PaperSettings");
	app.setApplicationVersion(QStringLiteral(PAPER_VERSION_STR));
	app.setDesktopFileName("org.cubocore.PaperSettings.desktop");
	app.setQuitOnLastWindowClosed(true);

	QCommandLineParser parser;

	parser.addHelpOption();         // Help
	parser.addVersionOption();      // Version

	/* Directly open a particular page */
	parser.addOption({ "page", "The settings page you want to open.", "page" });

	/* List the pages */
	parser.addOption({ "list-pages", "List the available settings pages.", "" });

	/* Process the CLI args */
	parser.process(app);

	if (parser.isSet("list-pages"))
	{
		std::cout << "Paper Settings" << std::endl;
		std::cout << "v" << PAPER_VERSION_STR << std::endl << std::endl;

		std::cout << "about      " << "Get information about your system" << std::endl;
		std::cout << "bluetooth  " << "Configure bluetooth" << std::endl;
		std::cout << "datetime   " << "Configure date and time" << std::endl;
		std::cout << "display    " << "Configure display - resolution, refresh rate, etc" << std::endl;
		std::cout << "home       " << "Configure ??" << std::endl;
		std::cout << "input      " << "Configure input devices - keyboard, mouse etc" << std::endl;
		std::cout << "network    " << "Configure the network" << std::endl;
		std::cout << "others     " << "Miscellaneous settings" << std::endl;
		std::cout << "personal   " << "Personalizations - Wallpaper, icon size, panel etc" << std::endl;
		std::cout << "power      " << "Comfigure the power manager" << std::endl;
		std::cout << "sound      " << "Configure the volume and mic" << std::endl;
		std::cout << "system     " << "Configure " << std::endl;
		std::cout << "users      " << "Configure users and groups" << std::endl;

		return 0;
	}

	Paper::SettingsUI g;

	QObject::connect(&app, &CPrime::CApplication::messageReceived, [&g]() {
		qDebug("Found a running instance of Paper Settings. Trying to communicate with it.");
		g.show();
		g.activateWindow();
	});

	if (parser.isSet("page"))
	{
		g.loadPage(parser.value("page"));
	}

	g.show();

	return app.exec();
}
