/*
  *
  * This file is a part of libpaperde.
  * Library for handling various services related to Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This file was originally a part of CoreApps project (https://gitlab.com/cubocore/coreapps)
  * Suitable modifications have been done to meet the needs of Paper Desktop.
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#pragma once

#include <QObject>
#include <QFont>
#include <QRect>

#include <paperde/paperglobals.h>

typedef QMap<QString, QVariant> SettingsMap;

namespace Paper {
class Settings;
}

class LIBPAPER_DLLSPEC Paper::Settings : public QObject {
	Q_OBJECT;

public:

	/*
	  * App will be one of the various apps:
	  * papershell, bluetooth, network, power, and so on...
	  */
	Settings(QString app);

	struct PaperProxy
	{
		QVariant value;

		operator int() const
		{
			return value.toInt();
		}

		operator uint() const
		{
			return value.toUInt();
		}

		operator float() const
		{
			return value.toReal();
		}

		operator double() const
		{
			return value.toDouble();
		}

		operator bool() const
		{
			return value.toBool();
		}

		operator QString() const
		{
			return value.toString();
		}

		operator QStringList() const
		{
			return value.value<QStringList>();
		}

		operator QSize() const
		{
			return value.toSize();
		}

		operator QFont() const
		{
			return value.value<QFont>();
		}

		operator QRect() const
		{
			return value.toRect();
		}
	};

	PaperProxy value(const QString& key);
	QVariant rawValue(const QString& key);
	void setValue(const QString& key, QVariant value);

Q_SIGNALS:
	void settingChanged(QString, QVariant);

private:
	QSettings *user, *system;
	QFileSystemWatcher *watcher;

	SettingsMap userMap;

private slots:
	void emitSettings(QSettings *);
};
