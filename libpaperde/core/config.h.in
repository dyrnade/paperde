/*
	*
	* This file is a part of libpaperde.
	* Library for handling various services related to Paper Desktop
	* Copyright 2020 CuboCore Group
	*

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

/** Current Version of Paper */
#define PAPER_VERSION_STR "@version@"

/** The root directory to which was Paper installed */
#define InstallPrefix "@INSTALL_PREFIX@/"

/** Location where system-wide shared data is available */
#define SharePath "@SHARED_DATA_PATH@/"

/** Location where global configs will be stored */
#define ConfigPath "@SHARED_DATA_PATH@/configs/"

/** Location where plugin metadata will be stored */
#define MetadataPath "@SHARED_DATA_PATH@/metadata/"

/** Location where plugins are installed */
#define PluginPath "@PLUGIN_PATH@/"

/** Location where Paper utilities are installed */
#define UtilsPath "@UTILS_PATH@/"
