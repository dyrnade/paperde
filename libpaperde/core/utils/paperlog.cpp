/*
  *
  * This file is a part of libpaperde.
  * Library for handling various services related to Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#include <QDir>
#include <QDebug>
#include <QDateTime>
#include <QFileInfo>

#include "paperlog.h"

FILE *Paper::log = nullptr;
bool Paper::SHOW_INFO_ON_CONSOLE     = true;
bool Paper::SHOW_DEBUG_ON_CONSOLE    = true;
bool Paper::SHOW_WARNING_ON_CONSOLE  = true;
bool Paper::SHOW_CRITICAL_ON_CONSOLE = true;
bool Paper::SHOW_FATAL_ON_CONSOLE    = true;

const char *Paper::COLOR_INFO     = "\033[01;32m";
const char *Paper::COLOR_DEBUG    = "\033[01;30m";
const char *Paper::COLOR_WARN     = "\033[01;33m";
const char *Paper::COLOR_CRITICAL = "\033[01;31m";
const char *Paper::COLOR_FATAL    = "\033[01;41m";
const char *Paper::COLOR_RESET    = "\033[00;00m";

void Paper::Logger(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
	QByteArray localMsg  = msg.toLocal8Bit();
	const char *file     = context.file ? context.file : "";
	const char *function = context.function ? context.function : "";

	switch (type)
	{
	case QtInfoMsg:
		if (Paper::SHOW_INFO_ON_CONSOLE)
		{
			fprintf(stderr, "%s[I]: (%s:%u, %s) %s%s\n",
					COLOR_INFO,
					file,
					context.line,
					function,
					localMsg.constData(),
					COLOR_RESET
					);
			fflush(stderr);
		}

		if (Paper::log)
		{
			fprintf(Paper::log, "[I]: (%s:%u, %s) %s\n", file, context.line, function, localMsg.constData());
			fflush(Paper::log);
		}

		break;

	case QtDebugMsg:
		if (Paper::SHOW_DEBUG_ON_CONSOLE)
		{
			fprintf(stderr, "%s[D]: (%s:%u, %s) %s%s\n",
					COLOR_DEBUG,
					file,
					context.line,
					function,
					localMsg.constData(),
					COLOR_RESET
					);
			fflush(stderr);
		}

		if (Paper::log)
		{
			fprintf(Paper::log, "[D]: (%s:%u, %s) %s\n", file, context.line, function, localMsg.constData());
			fflush(Paper::log);
		}

		break;

	case QtWarningMsg:
		if (Paper::SHOW_WARNING_ON_CONSOLE)
		{
			fprintf(stderr, "%s[W]: (%s:%u, %s) %s%s\n",
					COLOR_WARN,
					file,
					context.line,
					function,
					localMsg.constData(),
					COLOR_RESET
					);
			fflush(stderr);
		}

		if (Paper::log)
		{
			fprintf(Paper::log, "[W]: (%s:%u, %s) %s\n", file, context.line, function, localMsg.constData());
			fflush(Paper::log);
		}

		break;

	case QtCriticalMsg:
		if (Paper::SHOW_CRITICAL_ON_CONSOLE)
		{
			fprintf(stderr, "%s[E]: (%s:%u, %s) %s%s\n",
					COLOR_CRITICAL,
					file,
					context.line,
					function,
					localMsg.constData(),
					COLOR_RESET
					);
			fflush(stderr);
		}

		if (Paper::log)
		{
			fprintf(Paper::log, "[E]: (%s:%u, %s) %s\n", file, context.line, function, localMsg.constData());
			fflush(Paper::log);
		}

		break;

	case QtFatalMsg:
		if (Paper::SHOW_FATAL_ON_CONSOLE)
		{
			fprintf(stderr, "%s[#]: (%s:%u, %s) %s%s\n",
					COLOR_FATAL,
					file,
					context.line,
					function,
					localMsg.constData(),
					COLOR_RESET
					);
			fflush(stderr);
		}

		if (Paper::log)
		{
			fprintf(Paper::log, "[#]: (%s:%u, %s) %s\n", file, context.line, function, localMsg.constData());
			fflush(Paper::log);
		}

		break;
	}
}
